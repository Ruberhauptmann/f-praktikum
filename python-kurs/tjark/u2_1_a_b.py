import matplotlib.pyplot as plt
import numpy as np
from math import sqrt

hist = False

decays = [1,1,5,4,2,0,3,2,4,1,2,1,1,0,1,1,2,1]

if hist == True:
    plt.hist(decays, [-0.5,0.5,1.5,2.5,3.5,4.5,5.5])
    plt.show()

decay_sum = 0
for decay in decays:
    decay_sum += decay

mean_manual = decay_sum/len(decays)

std_sum = 0
for decay in decays:
    std_sum += (decay - mean_manual)**2

std_manual = sqrt(std_sum/(len(decays) - 1))

mean_np = np.mean(decays)
std_np = np.std(decays,ddof=1)

print("Mittelwert manuell: ", mean_manual)
print("Mittelwert mit numpy: ", mean_np)
print("Standardabweichung manuell: ", std_manual)
print("Standardabweichung mit numpy: ", std_np)
