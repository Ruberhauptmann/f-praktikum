# Approximation von pi mithilfe einer Monte-Carlo Methode
# Zufallsgenerierung von Zufallszahlen mit x <= 1, y <= 1
# Verhältnis Zahlen innerhalb des Kreises sqrt(x^2 + y^2) <= 1 und Zahlen im Quadrat = pi

from math import pi, sqrt
import random
import matplotlib.pyplot as plt

def approx_pi(n_random_numbers, n_tries=1):
    pi_approx = []
    error_sum = 0

    for j in range(0,n_tries):
        N_i = 0
        for i in range(0,n_random_numbers):
            x = random.random()
            y = random.random()

            if sqrt(x**2 + y**2) <= 1:
                N_i += 1

        pi_approx.append(4 * N_i / n_random_numbers)
        
    average = sum(pi_approx) / n_tries


    if sqrt((1 - average / pi)**2) < 0.001:
        print("Unsicherheit < 0.1% nach ", n_random_numbers, "Würfen.")

    for i in range(0,n_tries - 1):
        error_sum += (pi_approx[i] - average)**2

    error = sqrt(1/n_tries * error_sum)

    return average, error
    #print("Approximate pi = ", pi_approx)
    #print("Exact pi = ", pi)
    #print("Relative error = ", 1 - pi_approx/pi)


n_tries = 10
#print("How many numbers should be generated?")
#n_random_numbers = int(float(input()))

throws = [50, 100, 1000, 5000, 10000, 100000, 1000000]

print(throws)

approx_pis = []
errors = []
for i in throws:
    approx_pis.append(approx_pi(i,n_tries)[0])
    errors.append(approx_pi(i,n_tries)[1])


plt.errorbar(throws,approx_pis,yerr=errors)
plt.show()