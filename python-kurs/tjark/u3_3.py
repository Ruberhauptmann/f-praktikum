from math import sqrt,sin,cos
import numpy as np
import matplotlib.pyplot as plt
import random

part_a = False
part_b = False
part_c = False
part_d = True

def give_uniform(n,mu,sigma):
    a = np.array([[0.5,0.5],[-1/sqrt(12),1/sqrt(12)]])
    b = np.array([mu,sigma])
    x = np.linalg.solve(a,b)
    distribution = [random.uniform(x[0],x[1]) for i in range(0,n)]
    return np.array(distribution)

def give_uniform_sine(n,mu,sigma):
    return np.sin(give_uniform(n,mu,sigma))

def give_uniform_cosine(n,mu,sigma):
    return np.cos(give_uniform(n,mu,sigma))

def give_binomial(n,tries,p):
    distribution = [np.random.binomial(tries,p) for i in range(0,n)]
    return np.array(distribution)

def give_poisson(n,mu):
    distribution = [np.random.poisson(mu) for i in range(0,n)]
    return np.array(distribution)

def give_chi_2(n,dof):
    distribution = [np.random.chisquare(dof) for i in range(0,n)]
    return np.array(distribution)

def give_dist(dist,n,par1=0,par2=1):
    if dist == 'uniform':
        return give_uniform(n,par1,par2)
    if dist == 'sine':
        return give_uniform_sine(n,par1,par2)
    if dist == 'cosine':
        return give_uniform_cosine(n,par1,par2)
    if dist == 'binomial':
        return give_binomial(n,par1,par2)
    if dist == 'poisson':
        return give_poisson(n,par2)
    if dist == 'chi_2':
        return give_chi_2(n,par1)
    else:
        print(dist, "existiert nicht in der Datenbank, beep boop")
        return np.array([0])

##
# Part a
if part_a == True:
    mu = 0
    sigma = 1
    uniform_dist = give_uniform(1000,mu,sigma)

    print("Mittelwert: ", np.mean(uniform_dist))
    print("Standardabweichung: ", np.std(uniform_dist))

    plt.hist(uniform_dist,bins='auto')
    plt.show()

##
# Part b
if part_b == True:
    mu = 0
    sigma = 1
    sum_dist = []

    for i in range(0,1000):
        dist = give_dist('uniform',10,mu,sigma)
        sum_dist.append(sum(dist))
    
    print("Mittelwert: ", np.mean(sum_dist))
    print("Standardabweichung: ", np.std(sum_dist))

    plt.hist(sum_dist,bins='auto')
    plt.show()

##
# Part c
if part_c == True:
    mu = 0
    sigma = 1
    sum_dist_sine = []

    for i in range(0,1000):
        dist = give_dist('sine',10,mu,sigma)
        sum_dist_sine.append(sum(dist))
    
    print("Mittelwert sinus: ", np.mean(sum_dist_sine))
    print("Standardabweichung sinus: ", np.std(sum_dist_sine))

    plt.hist(sum_dist_sine,bins='auto')
    plt.show()
    plt.close()
    
    sum_dist_cosine = []

    for i in range(0,1000):
        dist = give_dist('cosine',10,mu,sigma)
        sum_dist_cosine.append(sum(dist))
    
    print("Mittelwert cosinus: ", np.mean(sum_dist_cosine))
    print("Standardabweichung cosinus: ", np.std(sum_dist_cosine))

    plt.hist(sum_dist_cosine,bins='auto')
    plt.show()
    plt.close()

##
# Part d
if part_d == True:
    dist_wanted = 'chi_2'
    sum_dist = []

    mu = 0
    sigma = 1
    # For binomial distribution
    tries = 100
    p = 0.5
    # For chi^2
    dof = 1

    for i in range(0,1000):
        if dist_wanted == 'binomial':
            dist = give_dist(dist_wanted,10,tries,p)
            sum_dist.append(sum(dist))
        if dist_wanted == 'chi_2':
            dist = give_dist(dist_wanted,10,dof,dof)
            sum_dist.append(sum(dist))
        
        else:
            dist = give_dist(dist_wanted,10,mu,sigma)
            sum_dist.append(sum(dist))
    
    print("Mittelwert: ", np.mean(sum_dist))
    print("Standardabweichung: ", np.std(sum_dist))

    plt.hist(sum_dist,bins='auto')
    plt.show()