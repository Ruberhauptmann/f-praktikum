# Monty-Hall Problem

import random


def game(n_games, switch_choice):
    win = 0

    for i in range(0,n_games):
        doors = [1,2,3]
        win_door = random.randint(1,3)
        candidate_choice = random.randint(1,3)

        doors.remove(candidate_choice)
        
        if candidate_choice == win_door:
            doors.remove(random.choice(doors))
        else:
            for i in doors:
                if i != win_door:
                    doors.remove(i)

        if switch_choice == True:
            candidate_choice = doors[0]

        if candidate_choice == win_door:
            win += 1
    
    return win

n_games = 1000

win_switch = game(n_games,True)
win_no_switch = game(n_games,False)

print("Gewinnwahrscheinlichkeit beim Wechseln:", win_switch/n_games)
print("Gewinnwahrscheinlichkeit ohne Wechseln:", win_no_switch/n_games)