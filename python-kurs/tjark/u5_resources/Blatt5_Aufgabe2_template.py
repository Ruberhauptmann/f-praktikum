import math
import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack
import scipy
import scipy.io.wavfile

def getSpectrum(y,SR):
    #Parameters: y: data to be transformed; SR: sampling rate (Abtastrate)
    #returns: frequency, FFT(data)
    n = len(y)
    k = np.arange(n)
    T = n/SR #period T = length / sampling rate
    frq = k/T #two sides frequency range

    Y = scipy.fftpack.fft(y)/n #fft computing and normalization
    return frq,Y

def getPower(Y):
    #transform Y into Schalldruckpegel (in decibel)
    Power = abs(Y) #Take absolute value to get magnitude of the frequency components
    Power = Power/float(len(Power)) #scale by number of points so that
                                    #magnitude does not depend on signal length or sampling frequency
    Power = Power**2  #square it to get the power 
    Power = 10*np.log10(Power) #take 10*log10() to get the the power in decibel (dB) (Schalldruckpegel)
    return Power

path = './' #anpassen
filename = 'mysound'
suffix = '.wav'
rate,data = scipy.io.wavfile.read(path+filename+suffix)
print('Data shape:',data.shape)

#The wav file has two channels
#Select and work with the first channel only:
y=data[:,1]
data = np.ones(5) #clear data array to save memory (not possible to delete objects in python)

#select only 50000 sample points
y=y[0:50000] #modify if needed

timp=len(y)/rate
print('Sampling rate =%d samples/sec'%rate)
print('Duration of signal: %.2f s'%timp)
t=np.linspace(0,timp,len(y))

#perform fourier transformation
frq, Y = getSpectrum(y,rate)
Power = getPower(Y)


#do stuff here...
#e.g. plot y vs t and Power vs frq
#e.g. cut frequency spectrum


#transform back (inverse FFT) and write to new wave file
#initialise array, dtype seems to be important for wave file
databack = np.zeros((len(Y),2),dtype=np.int16) 

yback = np.real(scipy.fftpack.ifft(Y)/len(Y))*2*10**4 # amplify signal, otherwise you won't hear anything; adapt if needed
databack[:,0] = yback
databack[:,1] = yback #preserve 2-channel structure of wav file

scipy.io.wavfile.write(path+filename+'_back'+suffix,rate,databack)

