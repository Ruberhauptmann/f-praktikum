import math
import numpy as np
import matplotlib.pyplot as plt

#define a square wave function with period T
def squareWave(x,T):
    def mySign(x): #numpy.sign(x) returns 0 for x==0, we need to return 1 for x==0
        if x==0:
            return 1
        else:
            return np.sign(x)
    #define square wave via a kind of elementray 'cell': y = -1 for -T/2 <= x < 0 and y = +1 for 0 <= x < +T/2
    #for arbitrary x shift by n*T so that it's back in -T/2 <= x < +T/2
    #then simply the sign of x gives y
    return mySign(x - T * int( x /float(T) + mySign(x) * 0.5 ))

x = np.linspace(-10,10,1000)
y = []
T = 4

for i in x:
    y.append(squareWave(i,T))

plt.plot(x,y,'b-')
plt.title("Square wave, T="+str(T))
plt.ylim([-1.5,1.5])
plt.show()
