import matplotlib.pyplot as plt
import numpy as np
from scipy.special import factorial
from math import exp

decays = np.array([1,1,5,4,2,0,3,2,4,1,2,1,1,0,1,1,2,1])

def give_poisson(mean, r):
    return mean**r / factorial(r) * np.exp(-mean)

mean = np.mean(decays)
#std = np.std(decays,ddof=1)

poisson = []
for i in range(0,6):
    poisson.append(give_poisson(mean,i))

n, bins, patches = plt.hist(decays, [-0.5,0.5,1.5,2.5,3.5,4.5,5.5])
print(n)

plt.close()

n = np.array(n)

plt.plot(range(0,6),poisson)
#plt.plot(range(0,len(decays)),decays/len(decays))
plt.plot(range(0,6), n/len(decays))
plt.show()