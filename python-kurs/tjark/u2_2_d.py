import numpy as np
import matplotlib.pyplot as plt
from math import sqrt,pi
#from scipy import integrate.quad

mu = 0
sigma_2 = 1

sigma_env = 2

plot = False

normal_3 = np.random.normal(mu,sigma_2,10**3)
normal_4 = np.random.normal(mu,sigma_2,10**4)
normal_5 = np.random.normal(mu,sigma_2,10**5)


fig,axes = plt.subplots(1,3)
n_3,bins_3,patches_3 = axes[0].hist(normal_3,bins='auto',density=False,range=[-sigma_env,sigma_env])
n_4,bins_4,patches_4 = axes[0].hist(normal_4,bins='auto',density=False,range=[-sigma_env,sigma_env])
n_5,bins_5,patches_5 = axes[0].hist(normal_5,bins='auto',density=False,range=[-sigma_env,sigma_env])

if plot == True:
    plt.show()

sum_3 = 0
for i in range(0,len(bins_3) - 1):
        sum_3 += n_3[i] / 10**3

sum_4 = 0
for i in range(0,len(bins_4) - 1):
        sum_4 += n_4[i] / 10**4

sum_5 = 0
for i in range(0,len(bins_5) - 1):
        sum_5 += n_5[i] / 10**5

print(sum_3 * 100)
print(sum_4 * 100)
print(sum_5 * 100)