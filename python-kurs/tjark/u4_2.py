from scipy.optimize import curve_fit
import scipy.integrate as integrate
from scipy.stats import chi2,poisson
from scipy.special import factorial
from math import sqrt
import numpy as np
import matplotlib.pyplot as plt

part_a = True
part_b = True

decays = [1,1,5,4,2,0,3,2,4,1,2,1,1,0,1,1,2,1]
bins = np.arange(max(decays) + 2) - 0.5

def give_poisson(r,mean):
    return mean**r / factorial(r) * np.exp(-mean)


if part_a == True: 
    [entries, bin_edges, patches] = plt.hist(decays,bins=bins,density=True)

    entries = np.array(entries)
    bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])
    parameters,cov_matrix = curve_fit(give_poisson,bin_middles,entries,sigma=np.sqrt(entries),absolute_sigma=True)
    print(np.diag(parameters))
    print(sqrt(np.diag(cov_matrix)))
    
    plt.plot(bin_middles,give_poisson(bin_middles,parameters[0]))
    plt.show()

#if part_b == True:
