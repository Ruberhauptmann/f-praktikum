# Approximation von pi mithilfe einer Monte-Carlo Methode
# Zufallsgenerierung von Zufallszahlen mit x <= 1, y <= 1
# Verhältnis Zahlen innerhalb des Kreises sqrt(x^2 + y^2) <= 1 und Zahlen im Quadrat = pi

from math import pi, sqrt
import random

part_b = True
part_c = True
part_d = True

def approx_pi(n_random_numbers):
    N_i = 0
    for i in range(0,n_random_numbers):
            x = random.random()
            y = random.random()

            if sqrt(x**2 + y**2) <= 1:
                N_i += 1

    pi_approx = 4 * N_i / n_random_numbers

    return pi_approx

print("Wie viele Paare sollen generiert werden?")
n_random_numbers = int(float(input()))

pi_approx = approx_pi(n_random_numbers)

print("Errechnetes Pi: ", pi_approx)
print("Literaturwert: ", pi)
print("Relative Abweichung: ", abs(pi - pi_approx)/pi)