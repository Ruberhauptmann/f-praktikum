import numpy as np
import matplotlib.pyplot as plt
from math import sqrt,pi
#from scipy import integrate.quad

mu = 0
sigma_2 = 1

plot = False

normal_3 = np.random.normal(mu,sigma_2,10**3)
normal_4 = np.random.normal(mu,sigma_2,10**4)
normal_5 = np.random.normal(mu,sigma_2,10**5)

def gauss(x,mu,sigma_2):
    return 1/sqrt(2*pi*sigma_2) * np.exp(- (x - mu)**2 / (2*sigma_2))

gauss_3 = []
gauss_4 = []
gauss_5 = []

fig,axes = plt.subplots(1,3)
n_3,bins_3,patches_3 = axes[0].hist(normal_3,bins='auto',density=True)
n_4,bins_4,patches_4 = axes[1].hist(normal_4,bins='auto',density=True)
n_5,bins_5,patches_5 = axes[2].hist(normal_5,bins='auto',density=True)

for i in bins_3:
    gauss_3.append(gauss(i,mu,sigma_2))
for i in bins_4:
    gauss_4.append(gauss(i,mu,sigma_2))
for i in bins_5:
    gauss_5.append(gauss(i,mu,sigma_2))

axes[0].plot(bins_3,gauss_3)
axes[1].plot(bins_4,gauss_4)
axes[2].plot(bins_5,gauss_5)

if plot == True:
    plt.show()
    plt.close()


print("Mittelwert für 10^3: ", np.mean(normal_3))
print("Standardabweichung für 10^3: ", np.std(normal_3,ddof=1))
print("Mittelwert für 10^4: ", np.mean(normal_4))
print("Standardabweichung für 10^4: ", np.std(normal_4,ddof=1))
print("Mittelwert für 10^5: ", np.mean(normal_5))
print("Standardabweichung für 10^5: ", np.std(normal_5,ddof=1))