# Convert Celsius to Fahrenheit
# 0°C -> 32°F, 100°C -> 212°F, 1°C = 1,8°F

part_a = False
part_b = True

def CtoF(temperature):
    return temperature * 1.8 + 32  

if part_a == True:
    print("Input Temperature in Celsius")
    temperature = int(input())

    print(temperature, "in Fahrenheit: ", CtoF(temperature))

if part_b == True:
    for i in range(0,101):
        print(i, "°C -> ", CtoF(i), "°F")