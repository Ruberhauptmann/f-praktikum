from scipy.optimize import curve_fit
import scipy.integrate as integrate
from scipy.stats import chi2
from math import sqrt
import numpy as np
import matplotlib.pyplot as plt

part_a = False
part_b = False
part_d = False
part_e = True

t = np.array([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0])
s = np.array([0.1,0.5,0.6,1.0,0.9,2.0,2.8,3.4,4.3,4.5])
error = np.full((10,), 0.3)
g = 9.81
linear_g = 0.5*np.multiply(g,t)
squared_g = 0.5*np.multiply(g,np.square(t))
dof = 10

def chi_2(x,sigma,X):
    return np.sum( np.square( np.divide(np.subtract(x,X),sigma) ))

if part_a == True:
    print("Chi^2 für linear: ", chi_2(s,error,linear_g)/10)
    print("Chi^2 für quadriert: ", chi_2(s,error,squared_g)/10)

if part_b == True:
    print("linear: ", 1 - integrate.quad(chi2.pdf,0,chi_2(s,error,linear_g),args=(dof))[0])
    print("quadriert: ", 1 - integrate.quad(chi2.pdf,0,chi_2(s,error,squared_g),args=(dof))[0])

if part_d == True:
    error_double = 2*error
    print("Chi^2 für linear: ", chi_2(s,error_double,linear_g)/10)
    print("Chi^2 für quadriert: ", chi_2(s,error_double,squared_g)/10)

    error_halve = 0.5*error
    print("Chi^2 für linear: ", chi_2(s,error_halve,linear_g)/10)
    print("Chi^2 für quadriert: ", chi_2(s,error_halve,squared_g)/10)

if part_e == True:
    def give_s(t,g):
        return 0.5 * g * t**2

    g_approx,g_covmat = curve_fit(give_s,t,s,sigma=error,absolute_sigma=True)
    
    s_fit = np.array([give_s(t,g_approx[0]) for t in t])
    error_fit = np.full((10,),sqrt(g_covmat[0]))
    print("g gefittet: ", g_approx[0] ,"+-", sqrt(g_covmat[0]))
    print(0.5 * np.multiply(g_approx[0],np.square(t)))
    print("Chi^2 für gefitted: ", chi_2(s_fit,error_fit,0.5 * np.multiply(g_approx[0],np.square(t))))

    """
    plt.plot(t,s_fit)
    plt.plot(t,s)
    plt.show()
    """