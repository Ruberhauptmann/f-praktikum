import math
import numpy as np
import matplotlib.pyplot as plt

def give_coefficient(n):
    return 2/math.pi * ( 1 - math.cos(n*math.pi))/n

def give_approx(n,x,T):
    #print([give_coefficient(i) * math.sin(2*i*math.pi/T * x) for i in range(1,n+1)])
    return np.sum([give_coefficient(i) * math.sin(2*i*math.pi/T * x) for i in range(1,n+1)])
    

#define a square wave function with period T
def squareWave(x,T):
    def mySign(x): #numpy.sign(x) returns 0 for x==0, we need to return 1 for x==0
        if x==0:
            return 1
        else:
            return np.sign(x)
    #define square wave via a kind of elementray 'cell': y = -1 for -T/2 <= x < 0 and y = +1 for 0 <= x < +T/2
    #for arbitrary x shift by n*T so that it's back in -T/2 <= x < +T/2
    #then simply the sign of x gives y
    return mySign(x - T * int( x /float(T) + mySign(x) * 0.5 ))

x = np.linspace(-10,10,1000)
T = 4
y_square = [squareWave(i,T) for i in x]
#y_2= [give_approx(2,i,T) for i in x]
#y_4 = [give_approx(4,i,T) for i in x]
#y_6 = [give_approx(6,i,T) for i in x]

approx_grade = 500
y_approx = [give_approx(approx_grade,i,T) for i in x]

plt.plot(x,y_square,'b-')
plt.plot(x,y_approx,'r-')
plt.title("Square wave, T="+str(T) + " Approximiert durch "+str(approx_grade)+" sinus-Funktionen")
plt.ylim([-1.5,1.5])
plt.show()