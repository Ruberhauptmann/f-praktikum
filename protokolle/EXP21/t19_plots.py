import numpy as np
import matplotlib.pyplot as plt

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

def flattop(x, sig):
    out = np.ones(x.shape)
    return np.heaviside(x + sig / 2, out) - np.heaviside(x - sig / 2, out) 

x_values = np.linspace(-3, 3, 120)

mu = 0
sig = 1
plt.plot(x_values, gaussian(x_values, mu, sig), label="Anregungsstrahl")

mu = 0
sig = 0.25
plt.plot(x_values, gaussian(x_values, mu, sig), label="Abfragestrahl")
plt.xlabel("Position auf der Probe [arb'U]")
plt.ylabel("Intensität [arb'U]")
plt.legend()
#plt.plot(x_values, flattop(x_values, sig))
plt.savefig("t19_plots.pdf")
