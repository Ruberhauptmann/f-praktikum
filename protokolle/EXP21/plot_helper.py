"""
"""

import matplotlib.pyplot as plt
import numpy as np
import matplotlib
matplotlib.rcParams.update({'font.size': 16})


def load_data(filepath, nheaderrows, delimiter=" "):
    data = np.genfromtxt(filepath, skip_header=nheaderrows, dtype=float, delimiter=delimiter).T
    return data

def plot(data, savepath, xcol, ycol, xlabel, ylabel):
    """
    
    Args:
        data:
        savepath:
        xcol:
        ycol:
        xlabel:
        ylabel:
    """
    plt.plot(data[xcol], data[ycol])
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    #plt.show()
    plt.savefig(savepath, bbox_inches="tight")
    plt.close()

def maximum(data):
    return np.amax(data)

def find_nearest(a, a0):
    """
    Return index of the element in array a closest to scalar value a0
    """
    idx = np.abs(a - a0).argmin()
    return idx

def calculate_fwhm(data, xcol, ycol):
    maximum_height = maximum(data[ycol])
    print("Maximum = {}".format(maximum_height))

    index_left = find_nearest(data[ycol], maximum_height / 2)
    print("Index Left {}".format(index_left))
    index_right = find_nearest(np.flip(data[ycol]), maximum_height / 2)
    print("Index Right {}".format(index_right))
    fwhm = data[xcol][index_right] - data[xcol][index_left]

    return np.abs(fwhm)


#if __name__ == "__main__":
    #plot('CRXO_Plots/cxrotransmission.txt','CRXO_Plots/cxrotransmission.pdf', 0, 1, 2, 'x', 'y')
