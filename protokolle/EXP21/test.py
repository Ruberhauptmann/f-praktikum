import os
import numpy as np
import matplotlib.pyplot as plt
from plot_helper import load_data, plot, calculate_fwhm

data = load_data("labordaten/Time_resolved_data.csv", 1, delimiter=";")
energies = data[0][1:]
time_delays = data.T[0][1:]
data = data.T[:,1:][1:]
print(data.shape)