"""
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.ticker import LogFormatter 
from plot_helper import load_data, plot, calculate_fwhm
import matplotlib
matplotlib.rcParams.update({'font.size': 16})

def plot_labdata():
    # Plot labdata
    vH_mittel = np.zeros(420)
    for filename in os.listdir("labdata"):
        metadata = filename.split("_")
        task = metadata[0]
        device = metadata[1]
        filepath = "labdata/" + filename
        print("Current file: {}".format(filepath))
        savepath = "labdata_plots/" + filename + ".pdf"

        data = load_data(filepath, 4)

        fwhm_tasks = ["t5", "t9"]
        if device == "spa1":
            xlabel = "Röntgenenergie $[eV]$"
            ylabel = "Intensität [arb'U]"

            if task in fwhm_tasks:
                print("FWHM = {} {}".format(calculate_fwhm(data, xcol=1, ycol=2), "eV"))

        if device == "biu":
            xlabel = "Position $[mm]$"
            ylabel = "Intensität [arb'U]"

            if task in fwhm_tasks:
                print("FWHM = {} {}".format(calculate_fwhm(data, xcol=1, ycol=2), "mm"))
        
        if task == "t39":
            xlabel = "Röntgenenergie $[eV]$"
            ylabel = "Intensitätsdifferenz [arb'U]"
            plt.xlim([7035,7075])

        if task != "t38":
            plot(data, savepath, xcol=1, ycol=2, xlabel=xlabel, ylabel=ylabel)

        # ipm data must be plotted separately
        if device == "apd1":
            data = np.genfromtxt(filepath, skip_header=4, dtype=float).T

            # intensity plot
            for i in range(2, 6):
                plt.plot(data[1], data[i], label=str(i))
            plt.xlabel("Zeit [ps]")
            plt.ylabel("Intensität [arb'U]")
            plt.legend()

            plt.savefig(savepath)
            plt.close()

            # position plot
            horizontal_columns = [2, 4]
            vertical_columns = [3, 5]

            horizontal_difference = (1 / data[horizontal_columns[1]] - 1 / data[horizontal_columns[0]]) * 10**5 
            vertical_difference = (1 / data[vertical_columns[1]] - 1 / data[vertical_columns[0]]) * 10**5 

            horizontal_relative = horizontal_difference - np.mean(horizontal_difference[1:])
            vertical_relative = vertical_difference - np.mean(vertical_difference)

            plt.plot(data[1], horizontal_difference, label="Horizontal")
            plt.plot(data[1], vertical_difference, label="Vertikal")

            plt.legend()

            savepath = "labdata_plots/" + filename + "_difference" + ".pdf"
            plt.savefig(savepath)
            plt.close()

            plt.plot(data[1], horizontal_relative, label="Horizontal")
            plt.plot(data[1], vertical_relative, label="Vertikal")

            plt.legend()

            plt.xlabel("Zeit [arb'U]")
            plt.ylabel("Abweichung [arb'U]")

            savepath = "labdata_plots/" + filename + "_relative_difference" + ".pdf"
            plt.savefig(savepath, bbox_inches="tight")
            plt.close()

            # Scatter plot
            plt.scatter(horizontal_relative, vertical_relative)

            plt.xlabel("Horizontale Position [arb'U]")
            plt.ylabel("Vertikale Position [arb'U]")

            plt.axvline(color="black")
            plt.axhline(color="black")

            savepath = "labdata_plots/" + filename + "_difference_scatter" + ".pdf"
            plt.savefig(savepath, bbox_inches="tight")
            plt.close()
        
        # von Hamos Data
        if task == "t38":
            vH_energy = data[1]
            vH_mittel = vH_mittel + data[2]/10

    plt.plot(vH_energy, vH_mittel)
    plt.xlim([7035,7075])
    plt.xlabel("Röntgenenergie $[eV]$")
    plt.ylabel("Intensität [arb'U]")

    plt.savefig("labdata_plots/t38_vH_mittel.pdf", bbox_inches="tight")
    plt.close()


    return None

def plot_cxrodata():
    # Plot CXRO data
    for filename in os.listdir("cxro_data"):
        filepath = "cxro_data/" + filename
        savepath = "cxro_plots/" + filename + ".pdf"
        print("Current file: {}".format(filepath))
        data = load_data(filepath, 2)
        plot(data, savepath, xcol=0, ycol=-1, xlabel="Röntgenenergie $[eV]$", ylabel="Transmission")

    return None

def plot_xfeldata():
    # Plot real XFEL data
    for filename in os.listdir("labordaten"):
        extension = filename.split(".")[-1]
        filepath = "labordaten/" + filename
        savepath = "labordaten_plots/" + filename + ".pdf"  
        print("Current file: {}".format(filepath))

        if filename == "Reference_Data.csv":
            data = load_data(filepath, 1, delimiter=";")
            data[1:] = data[1:] / np.amax(data[1])
            zustand = ["Singulett" , "Duplett" , "Triplett" , "Quartett" , "Quintett"]
            for i in range(1, 6):
                plt.plot(data[0], data[i], label = zustand[i-1])

            plt.xlabel("Röntgenenergie [eV]")
            plt.ylabel("norm. Intensität")
            plt.legend()

            plt.savefig(savepath, bbox_inches="tight")
            plt.close()

            for i in range(2, 6):
                plt.plot(data[0], data[i] - data[1], label= zustand[i-1])
            
            plt.xlabel("Röntgenenergie [eV]")
            plt.ylabel("$\Delta$ norm. Intensität")
            plt.legend()

            savepath = "labordaten_plots/" + filename + "_difference" + ".pdf"
            plt.savefig(savepath, bbox_inches="tight")
            plt.close()


        if filename == "Time_resolved_data.csv":
            # Waterfall Plot
            data = load_data(filepath, 2, delimiter=";")
            #data[1:] = data[1:] / np.amax(data[1]) #für Normierung: eigentlich mit Maximum von der spätestesten Zeit? -> entspricht aber auch nicht Einheit von Referenz
            time = load_data(filepath, 1, delimiter=";")
            auswahl = [1, 12, 14, 16, 20, 44]
            for i in auswahl:
                label = str(time.T[0][1:][i-1]) + " ps"
                plt.plot(data[0], data[i], label=label)

            plt.xlabel("Röntgenenergie $[eV]$")
            plt.ylabel("$\Delta$ Intensität [arb'U]")
            plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', title="Zeitdifferenz")

            plt.savefig("labordaten_plots/" + filename + "_waterfall" + ".pdf", bbox_inches="tight")
            plt.close()

            # Vergleich von zwei Zeiten
            auswahl3 = [12, 44]
            a, b = data.shape
            for i in auswahl3:
                #if 7040 < energies[i] < 7065:
                 #   label = str(energies[i]) + " $eV$"
                  #  plt.plot(time_delays, np.abs(data[i]/np.max(np.abs(data[i]))), label=label)
                label = str(time.T[0][1:][i-1]) + " ps"
                plt.plot(data[0], data[i] / np.max(np.abs(data[i])), label=label)
            
            plt.xlabel("Röntgenenergie [eV]")
            plt.ylabel("$\Delta$ norm. Intensität")
            plt.legend(title="Zeitdifferenz")
            plt.savefig("labordaten_plots/" + filename + "_waterfall_vgl" + ".pdf", bbox_inches='tight')
            plt.close()

            # False Color Plot
            data = load_data(filepath, 1, delimiter=";")
            energies = data[0][1:]
            time_delays = data.T[0][1:]
            data = data[:,1:][1:]

            pcm = plt.pcolormesh(energies, time_delays, data, shading="auto", cmap="RdBu_r", norm=colors.SymLogNorm(base=10, linthresh=0.1, linscale=0.1, vmin=-1.0, vmax=1.0))
            formatter = LogFormatter(10, labelOnlyBase=False)
            cbar = plt.colorbar(pcm, extend="both", format=formatter)
            cbar.set_label("Intensität [arb'U]")
            plt.xlabel("Röntgenenergie [eV]")
            plt.ylabel("Zeitdifferenz [ps]")

            plt.savefig("labordaten_plots/" + filename + "_falsecolor" + ".pdf", bbox_inches="tight")
            plt.close()

            # Kinetic Traces
            data = load_data(filepath, 1, delimiter=";")
            energies = data[0][1:]
            time_delays = data.T[0][1:]
            data = data.T[:,1:][1:]
            traces_colors = {0: "lightcoral", 73: "sienna", 142: "darkorange", 150: "gold", 160: "yellowgreen", 168: "lightseagreen", 175: "dodgerblue", 180: "darkblue", 190: "plum", 194: "crimson", 284: "black"}
            auswahl = [0, 73, 142, 150, 160, 168, 175, 180, 190, 194, 284]
            for i in auswahl:
                label = str(energies[i]) + " $eV$"
                plt.plot(time_delays, data[i], label=label, color=traces_colors[i])

            plt.xlabel("Zeit [ps]")
            plt.ylabel("Intensität [arb'U]")
            plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', title="Röntgenenergie")
            plt.savefig("labordaten_plots/" + filename + "_kinetic_traces" + ".pdf", bbox_inches='tight')
            plt.close()

            # Kinetic Traces Vergleich
            #plt.plot(time_delays, data[73]/np.amax(data[73]), label=str(energies[73])+" $eV$")
            #plt.plot(time_delays, data[180]/np.amax(data[180]), label=str(energies[180])+" $eV$")
            auswahl2 = [73, 142, 150, 160, 168, 175, 180]
            a,b = data.shape
            for i in auswahl2:
                #if 7040 < energies[i] < 7065:
                 #   label = str(energies[i]) + " $eV$"
                  #  plt.plot(time_delays, np.abs(data[i]/np.max(np.abs(data[i]))), label=label)
                label = str(energies[i]) + " $eV$"
                plt.plot(time_delays, np.abs(data[i] / np.max(np.abs(data[i]))), label=label, color=traces_colors[i])
            
            plt.xlabel("Zeit [ps]")
            plt.ylabel("norm. Intensität")
            plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', title="Röntgenenergie")
            plt.savefig("labordaten_plots/" + filename + "_kinetic_traces_vgl" + ".pdf", bbox_inches='tight')
            plt.close()



if __name__ == "__main__":

    #plot_labdata()

    #plot_cxrodata()

    plot_xfeldata()
