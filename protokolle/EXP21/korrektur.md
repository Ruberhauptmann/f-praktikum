done 1. Abb. 2: "im 3d Orbital des Metalls" - bitte streichen. (Genau genommen handelt es sich um Molekülorbitale mit Fe 3d Charakter.)	

done 2. Auf Seite 5: CT einmal als Charge Transfer (Ladungstransfer) ausschreiben.	

done 3. Auf Seite 6: dass sich in einem d-Orbital des Metalls befindet. Streng genommen ein Molekülorbital mit Metall (Eisen) d-Orbitalchrakter.	

done 4. Auf Seite 6: Internal Crossing existiert nicht = es müsste Intersystem Crossing heissen. Nochmal auf die Begrifflichkeiten ISC und IC auf dieser Seite 6 und deren Verwendung achten: (Infos z.B.  Innere Umwandlung – Wikipedia).	

done 5. Abb. 4: Potentialflächen von was? des Fe(trisbpy)_2^3+. Bildunterschrift ungenau. Genauso wie der Satz auf Seite 7, Abregungsprozesse laufen auf sehr kurzen Zeitskalen ab. Für manche Moleküle nicht, nur für das Fe(trisbpy)_2^3+ z.B.. Genauer formulieren.														

done 6. Seite 7: Röntgenanregung ins Kontinuum = Ionisation	

done 7. Seite 7 unten: Die Röntgenemssionslinien sind Element-charakteristisch, man könnte das um das Röntgenabsorptionspektrum und die jeweiligen Röntgenemissionslinien erweitern. ?was meint er da? Abbildung 2.3 aus der Mappe? Aber warum in dem Kontext? Und Emissionslinien sind im Skript nur 2.4, das hat aber nichts mit der Elementspezifität zu tun... 

done 8. Tippfehler am Anfang von 2.6.1..	

done 9. Seite 14: Ausdehnung des monochromatischen Röntgenstrahles im Vergleich zum pinken.	what? vielleicht der Grund für die unterschiedliche Ausdehnung?

done 10. Seite 19: Es sind tatsächlich Linsenkasetten.	

done 11. Seite 22: "Wie man ausserdem sieht, hat Wasser bei diesen Energien keine Absorptionskanten, es beeinflusst also nciht das Röntgenemissionspektrum bei diesen Energien." Missverständlich, was ihr sagen wollt ist wahrscheinlich: "Wasser hat keine Röntgenemissionslinien bei den betrachteten Röntgenenergien im Bereich xx-xx keV.	

done 12. Seite 26 unten: das Signal. - Es handet sich um ein elast. Streusignal.

done 13. Abb. 24: Elast. Röntgenstreusignal.

done 14. Abb. 23-30: Bildunterschriften präzisieren: z.B. Abb. 23 Röntgenemissionsrefenzspektren, welche Linien? Welches Element?  oder Abb. 24Welche Linien? Welches Element? Welches Molekül? Welches Lösungsmittel? Der Leser muss erkennen beim Blick auf die Bilder, was dargestellt ist.

Abb 29/30 done, noch andere? 15. Bildlegenden spezifizieren: z.B. Abb. 29: Röntgenenergie ist farblich kodiert.

done abb 24 bbox tight
