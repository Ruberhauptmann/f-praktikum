import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-1, 1, num=1000)
y_s = 10 * (1 + x**2)
y_t = (1 - x)**(-2)

plt.plot(x, y_s)
plt.ylabel("$\\frac{\mathrm{d \sigma}}{\mathrm{d} \Omega}\;$ [arbitrary units]", fontsize=15)
plt.xlabel("$\cos{\Theta}$", fontsize=15)
plt.savefig("skripte/plots/pre_plot_s.pdf", dpi=500, bbox_inches="tight")
plt.close()

plt.plot(x, y_s, label="s-channel")
plt.plot(x, y_t, label="t-channel")
plt.ylim(0, 25)
plt.ylabel("$\\frac{\mathrm{d \sigma}}{\mathrm{d} \Omega}\;$ [arbitrary units]", fontsize=15)
plt.xlabel("$\cos{\Theta}$", fontsize=15)
plt.legend()
plt.savefig("skripte/plots/pre_plot_s_t.pdf", dpi=500, bbox_inches="tight")
plt.close()
