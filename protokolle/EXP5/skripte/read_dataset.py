import csv
import numpy as np

def read_dataset(filename):
    with open(filename, 'r', newline='') as csvfile:
        reader = csv.reader(filter(lambda row: row[0]!='#', csvfile),
                            quoting=csv.QUOTE_NONNUMERIC, delimiter=',')
        data = np.array(list(reader))
    return data


if __name__ == '__main__':
    data = read_dataset('/home/jolange/private/teaching/fprakt_EXP5/data/csv/mm.csv')
    __import__('IPython').embed()