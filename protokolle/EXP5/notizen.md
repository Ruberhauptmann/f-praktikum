# Notizen

## Schnitte
### Elektron:
- Ctrk(N) <= 3 --> qq, (tt)
- Ctrk(sumP) >= 30 --> (tt)
- Ecal(sumE) >= 60 --> mm, tt()
- Hcal(sumE) <= 12 --> qq
### Myonen:
- Ctrk(N) = 2 --> qq, (tt)
- Ctrk(sumP) >= 30, <= 96 -->qq, tt,  ((ee))
- Ecal(sumE) <= 3.5 --> ee, qq, tt()
- Hcal(sumE) <= 9 --> (qq), (tt)
### Hadronen:
- Ctrk(N) >= 8 <= 85 --> ee, mm, tt
- Ctrk(sumP) kein Schnitt sinnvoll
- Ecal(sumE) >=35 <= 70 --> (ee), mm, ((tt))
- Hcal(sumE) >=7 <= 24 --> (ee), (mm), (tt)
### Tau:
- Ctrk(N) >=2 <=6 --> qq 
- Ctrk(sumP) >= 20 <= 75 --> mm
- Ecal(sumE) <= 66 --> ee(), mm
- Hcal(sumE) kein Schnitt sinnvoll

http://opal.web.cern.ch/Opal/tour/detector.html
pdg z-boson seite