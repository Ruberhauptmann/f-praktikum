\documentclass[12pt]{article}

\include{preamble}

\title{KV3: Kalibrierung eines Rastertunnelmikroskops}
\author{Tjark Sievers, Balduin Letzer}
\date{\today}

\begin{document}

\maketitle
\thispagestyle{empty}
\clearpage

\begin{abstract}
In diesem Versuch soll ein Rastertunnelmikroskop benutzt werden, um die atomare Oberfläche einer Probe abzubilden.
Durch Vergleich der gemessenen Atomabstände mit bekannten Parametern der Probe kann dann das Mikroskop kalibriert werden.
\end{abstract}
\newpage

\tableofcontents
\newpage

\section{Einleitung}
In der Rastersondenmikroskopie wird eine Sonde verwendet um zeilenweise eine Probenoberfläche zu untersuchen.
Dabei werden verschiedene Wechselwirkungen zwischen Sonde und Probe genutzt um unterschiedliche Informationen
über die Probe zu gewinnen.
Ein Rastertunnelmikroskop (RTM) nutzt den Tunnelstrom zwischen Sonde und Probe und kann dadurch Aufschluss über die 
elektronischen Zustandsdichte auf der Probenoberfläche geben.
In diesem Versuch wird hochorientiertes pyrolytisches Graphit (HOPG) benutzt um ein Rastertunnelmikroskop zu kalibrieren.
Dazu wird zunächst die Oberfläche des HOPG mit dem Rastertunnelmikroskop vermessen. Die daraus gewonnenen Messwerte
werden mit den bekannten Gitterparametern des HOPG verglichen und es kann ein Korrekturfaktor berechnet werden.

\section{Theoretische Grundlagen}
\subsection{Tunneleffekt}
%TODO: 
%Abbildungen
%Quelle für Herleitung (Buch von Wiesendanger?)
Physikalische Grundlage für Messungen eines RTM ist der Tunneleffekt. Hierbei können beispielsweise Elektronen
durch Potentialbarrieren tunneln, die laut den Gesetzen der klassischen Physik nicht überwindbar wären. Im Fall des RTM wird speziell
das Tunneln von Elektronen zwischen zwei Metallen betrachtet, die durch durch einen Spalt von wenigen Angström getrennt sind. 

Im einfachen Modell des freien Elektronengases lassen sich Metalle durch ihre Fermi-Energie \(E_F\) und die Austrittsarbeit \(\Phi\) beschreiben. Bei 
\(T = \SI{0}{\kelvin}\) sind alle Elektronenzustände unterhalb von \(E_F\) besetzt, es müssen also Elektronen die Austrittsarbeit \(\Phi\) zwischen der Fermi-Energie und 
dem Vakuum-Niveau überwinden. Werden nun zwei Metalle (mit i.A. unterschiedlichen Fermi-Energien) auf einem Abstand von wenigen Angström
aneinandergebracht, können Elektronen  aufgrund des Tunneleffektes aus dem Metall mit der höheren Fermi-Energie in unbesetzte Zustände des Metalls mit der niedrigeren 
Fermi-Energie tunneln, d.h. es fließt ein Strom (Tunnelstrom) zwischen den Metallen (siehe Abb. \ref{fig:tunnel_ohne_spannung}). Dies geschieht so lange, bis sich die Fermi-Niveaus 
angeglichen haben. 
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/tunnel_ohne_spannung.png}
    \caption{a) Metall im Modell des freien Elektronengases b) Tunnelstrom zwischen Metallen mit unterschiedlichen Fermi-Niveaus c) Angeglichene Fermi-Niveaus \cite{krause_2020}}
    \label{fig:tunnel_ohne_spannung}
\end{figure}

Nun kann eine externe Spannung \(U\) (Abb. \ref{fig:tunnel_externe_spannung}) angelegt werden, die dazu führt, dass eines der Fermi-Niveaus aus dem Gleichgewicht gebracht wird,
damit wieder höher liegt als das Fermi-Niveau des anderen Metalls und besetzte Zustände in einem der Metalle unbesetzten Zustände im anderen Metall gegenüberstehen. 

Bei Umpolung der Spannung wird dann das Fermi-Niveau im zweiten Metall runtergesetzt, der Tunnelstrom fließt dann also in die andere Richtung.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/tunnel_externe_spannung.png}
    \caption{Anlegen einer Spannung an zwei Metalle mit Umpolung in b) \cite{krause_2020}}
    \label{fig:tunnel_externe_spannung}
\end{figure}

Für den Betrieb eines RTM ist es wichtig zu wissen, wie der Tunnelstrom von dem Abstand zwischen den Metallen, also der Breite der Potentialbarriere \(s\) 
abhängt. Dazu muss die
Schrödingergleichung in drei Bereichen gelöst werden: vor, in und hinter der Potentialbarriere. Real handelt es sich um ein trapezförmiges
Potential mit den beiden Austrittsarbeiten \(\Phi_1\) und \(\Phi_2\) als linke und rechte Grenzen des Potentials. Ein gutes Näherungsmodell 
ist ein Rechteckpotential, für dessen Höhe das Mittel \(\Phi_m = \frac{\Phi_1 + \Phi_2}{2}\) gewählt wird. Für eine feste Elektronenenergie sind 
die Lösungen für die drei Bereiche dann eine einlaufende und eine reflektierte im 1. Bereich, exponentiell gedämpfte einlaufende und 
reflektierte Wellen im 2. Bereich sowie eine auslaufende Welle im 3. Bereich. Aus den Lösungen für die Schrödingergleichung 
ergibt sich der Transmissionskoeffizient \(T\) (in der Näherung einer stark dämpfenden Potentialbarriere, also \(\kappa s \gg 1\)):
\begin{equation}
    T \approx \frac{16 k^2 \kappa^2}{(k^2 + \kappa^2)^2} \cdot \exp{(-2\kappa s)}
\end{equation}
Hierbei ist \(\kappa = \sqrt{\frac{2m_e}{\hbar^2} \left(\frac{\Phi_1 + \Phi_2}{2} + \frac{e U}{2}\right)}\) die Zerfallsrate und \(k\) die die Wellenzahl der Elektronenwelle. 
Der Tunnelstrom \(I\) setzt sich zusammen aus tunnelnden Elektronen aus dem gesamten Energiefenster \(E \in \vert E_{F1} - E_{F2} \vert\), näherungsweise genügt es aber, 
nur die Elektronen an der Fermi-Energie \(E_{F1}\) zu betrachten, da für diese die Potentialbarriere am kleinsten ist und damit die Tunnelwahrscheinlichkeit am größten. Somit
ergibt sich die Proportionalität:
\begin{equation}
    I \propto T(E_{F1}) = I_0 \cdot \exp{(-2\kappa s)}
    \label{eq:Tunnelstrom}
\end{equation}

\subsection{Probe: HOPG} \label{section:ProbeTheo}
% TODO:
% Abbildungen
% Unterschied Isolator, Halbleiter, Metall?
% Auswirkung Unterschied alpha beta atome auf elektronendichte
Um das RTM zu kalibrieren, wird HOPG genutzt, ein Probenmaterial, dessen Gitterparameter durch beispielsweise Streuexperimente bereits sehr genau bekannt sind. Es besteht
aus Kohlenstoff, dessen 4 äußeren Elektronen die elektronischen Eigenschaften von HOPG bestimmt: Die \(2s\), \(2p_x\) und \(2p_y\) Wellenfunktionen überlappen 
sich und bilden \(sp^2\) Hybride, es bilden sich also kovalente Bindungen innerhalb der Ebenen aus. Das \(p_z\) Orbital formt \(\pi\)-Bindungen zwischen den Ebenen.
Die kovalenten Bindungen innerhalb der Schichten haben deutlich größere Bindungsenergien als die Van-der-Waals Bindungen zwischen den Ebenen.

HOPG liegt vor als Honigwabengitter mit Gitterkonstanten \(a = \SI{1.42}{\angstrom}\) und \(c = \SI{6.67}{\angstrom}\) sowie in einer ABA-Stapelfolge. Diese Schichtung
resultiert in zwei unterschiedlichen Arten von Oberflächenatomen: \(\alpha\)-Punkte haben ein direkt ein weiteres
Atom in der nächsten Schicht unter sich, \(\beta\)-Punkte erst in der übernächsten Ebene. Dadurch ist die Elektronendichte der \(\alpha\)-Atome von der Oberfläche 
weg verschoben, die \(\beta\)- und \(\alpha\)-Atome werden deshalb unterschiedlich vom RTM dargestellt. 
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/hopg_schematisch.png}
    \caption{Schematischer Aufbau der ABA-Schichtung von HOPG \cite{PhysRevB.35.7790}}
    \label{fig:hopg_schematisch}
\end{figure}

\section{Experimenteller Aufbau und Durchführung}
%TODO:
%Bilder und Text auf Bilder beziehen
\subsection{Aufbau eines RTM}
In einem RTM wird mit einer Sonde die Oberfläche einer Probe abgetastet. Dadurch entstehen besondere Anforderungen an die Sonde und die 
Halterung von Sonde und Probe.

\subsubsection{Die Sonde} \label{section:Sondenprep}
Die Sonde soll die Probenoberfläche atomar auflösen. Das bedeutet, dass sie eine einatomige Spitze haben muss.
Realisiert wird dies, indem ein Draht an einem Seitenschneider zerrissen wird.
Die so entstehende Spitze ist feiner als bei einem geschnittenen Draht, aber immernoch ungleichmäßig und vergleichsweise dick.
Deshalb eignet sich diese Art von Spitze nur zur Untersuchung von vergleichsweise ebenen Oberflächen, wie die des HOPG. Bei solchen Oberflächen tragen dann nach 
Formel (\ref{eq:Tunnelstrom}) nur noch die Atome am dichtesten zur Probe (idealerweise nur ein Einziges) relevant zum Tunnelstrom bei, die Form der höheren Schichten hat also 
keinen Einfluss mehr auf das Ergebnis.

In diesem Versuch wird ein \(\SI{250}{\micro \m}\) dicker 80\%Pt-20\%Ir-Draht werwendet, da Platin als Edelmetall sehr korrosionsbeständig ist und durch 
das Iridium die Härte des Drahtes erhöht wird.

\subsubsection{Die Probe} \label{section:Probenprep}
Die Probe sollte möglichst glatt und rein sein.
In diesem Versuch wird HOPG als Probe verwendet. Das HOPG liegt als Einkristall vor und ist weniger als ein Millimeter dünn und ungefähr quadratisch mit wenigen Millimetern
Kantenlänge. Die Probe ist mit doppelseitigen Klebeband auf einem metallischen Probenträger befestigt und mit Silberleitlack an den
Probenträger und damit an das RTM elektrisch angeschlossen. 

Um die Probe zu präparieren, wird mit einem Klebestreifen ein Teil der Oberfläche abgezogen.
Dadurch werden ganze Schichten entfernt, da wie in Teil (\ref{section:ProbeTheo}) erklärt, die Bindungen zwischen den Schichten im Vergleich zu den 
Bindungen innerhalb der Schichten wesentlich schwächer sind. 
Die neue Oberfläche ist dann zum ersten Mal der Luft ausgesetzt, also noch glatt und rein.

\subsubsection{Bewegung von Probe und Sonde} \label{section:Antriebe}
%Besserer Titel?, grafiken aus mappe
Um die Oberfläche der Probe untersuchen zu können, muss zunächst die Probe mit einem Grobantrieb an die Sonde herangebracht werden 
und dann die Sonde über die Probe bewegt werden können.
Dazu werden piezoelektrische Materialien verwendet.

Der Grobantrieb besteht aus einem Saphirprisma, das durch Piezo-Scheraktoren gehalten wird. Auf dem Prisma befindet sich der Probenträger.
Wird langsam eine Spannung an die Aktoren angelegt, scheren diese aus und das Prisma wird wenige zehn Nanometer zur Sonde bewegt. Dieser Bereich der Bewegung wird Stick-Bereich genannt. 
Wird die Spannung plötzlich ausgeschaltet, kehren die Aktoren ruckartig in ihre Ausgangposition zurück. Aufgrund seiner Trägheit kann das Prisma 
dieser Bewegung allerdings nicht folgen und es bleibt in der erhöhten Position. Die Aktoren rutschen also am Prisma herunter, der Bereich wird deshalb auch Slip-Bereich genannt.
Das Prinzip des Großantriebs ist in Abbildung \ref{fig:Grossantrieb} graphisch dargestellt.

Legt man also eine Sägezahnspannung an die Aktoren an, wird die Probe langsam an die Sonde herangebracht. Dabei kann die Spannung und die Frequenz der Sägezahnspannung nicht beliebig erhöht werden um die Geschwindigkeit
des Grobantriebes zu erhöhen. Steigt die Spannung zu schnell, scheren die Aktoren zu schnell aus und das Prisma kann nicht folgen. Genauso folgt das Prisma 
auch den Aktoren wieder nach unten, wenn die Spannung zu langsam abfällt.
Mit Frequenzen von mehreren Kilohertz lassen sich dann Geschwindigkeiten von bis zu einem Millimeter pro Sekunde erreichen.

\begin{figure}[H] 
  \centering
  \includegraphics[width=0.8\textwidth]{bilder/Grobantrieb.PNG}
  \caption{Funktionsweise des Grobantriebes, a) Stick-Bereich, b) Probe ist erhöht, c) Slip-Bereich, Probe bleibt erhöht \cite{krause_2020}}
  \label{fig:Grossantrieb}
\end{figure}

Die Sonde wird mit einem Röhrenscanner bewegt. Wie in Abbildung \ref{fig:Roehrenscanner} sichtbar, besteht dieser aus einem piezoelektrischen Röhrchen, an dem fünf Elektroden befestigt sind.
Diese bestehen aus einer metallischen Innenfläche des Röhrchens und einer in vier Segmente unterteilten metallischen Außenfläche.
Wird nun an eine der äußeren Elektroden eine Spannung angelegt, biegt sich das Röhrchen in die entsprechende Richtung. Diese Effekt kann noch verstärkt 
werden, indem an die gegenüberliegende Elektrode noch eine entgegengesetzte Spannung angelegt wird. Genauso streckt sich das gesamte Röhrchen, wenn an die 
innere Elektrode eine Spannung angelegt wird, bzw. noch eine entgegengesetzte Spannung an die Äußeren.
Es entsprechen also zwei der äußeren Elektroden der x-Richtung, die zwei Anderen der y-Richtung und die Innere der z-Richtung.
Dadurch ist die Sonde in allen drei Raumrichtung beweglich.

\begin{figure}[H] 
  \centering
  \includegraphics[width=0.4\textwidth]{bilder/Röhrenscanner.PNG}
  \caption{Aufbau eines Röhrenscanners \cite{krause_2020}}
  \label{fig:Roehrenscanner}
\end{figure}

\subsection{Betrieb des RTM}
Das RTM wird für diesen Versuch im Konstant-Strom-Modus benutzt. Der zu untersuchende Bereich der Probenoberfläche wird zunächst in ein Raster unterteilt. Dann wird jede Zeile in x-Richtung 
einmal hin und wieder zurück abgefahren. Danach wird die Spitze einen Schritt in die y-Richtung verschoben und die nächste Zeile abgefahren.
Es gibt also eine schnelle (x-Richtung) und eine langsame (y-Richtung) Richtung. 
Dabei wird die z-Auslenkung der Spitze durch einen Regelkreis so kontrolliert, dass der gemessene Tunnelstrom konstant gehalten wird.
Die Steuerspannung der z-Komponente wird dann aufgezeichnet und liefert die Informationen über die Probenoberfläche.

\subsection{Experimenteller Aufbau}
In diesem Versuch steht das RTM auf einem schwingungsgedämpften Tisch und ist mit einem Mess-Rechner verbunden.
Dieser steuert das RTM und nimmt außerdem die Daten auf. Außerdem wird ein Oszilloskop verwendet um den Tunnelstrom und die Spannung an der z-Elektrode 
anzuzeigen. Die Probe wird auf einem Probenträger in eine Proben-Schublade geklemmt, die mit einem Großantrieb bewegt werden kann.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/aufbau.png}
    \caption{Aufbau des RTM \cite{krause_2020}}
    \label{fig:aufbau}
\end{figure}

\section{Durchführung und Ergebnisse}

\subsection{Präparation von Tunnelspitze und Probe}
Tunnelspitze und Probe werden so, wie in den Abschnitten \ref{section:Sondenprep} und \ref{section:Probenprep} beschrieben präpariert.
Die Tunnelspitze wird dann in die Sacklochbohrung des Spitzenhalters eingesetzt, welcher wiederum in die Spitzenaufnahem des RTM eingesetzt wird.
Die Probe wird mit dem Probenträger in die Probenschublade des RTM eingeschoben.
Sowohl die Tunnelspitze als auch die Probe müssen regelmäßig neu präpariert werden, da sich nach längerer Zeit an der Luft Fremdatome anlagern können, welche 
die Leitfähigkeit und Struktur von Spitze und Probe deutlich verschlechtern kann. Außerdem kann es bei früheren Messungen zu gewollten oder ungewollten Probenkontakten 
gekommen sein, was auch wieder die Form der Spitze verschlechtern oder die zu untersuchenden Strukturen auf der Probenoberfläche zerstören kann. 

\subsection{Annäherung in den Tunnelkontakt}
Um den Tunnelkontakt herzustellen, wird zunächst die Probe mit dem in Abschnitt \ref{section:Antriebe} beschriebenen Grobantrieb auf ungefähr einen Viertel Millimeter
angenähert. Zur Kontrolle des Abstandes wird dabei ein optisches Mikroskop verwendet.
Für die restliche Annäherung wird dann eine Automatik benutzt. Diese fährt den Röhrenscanner so weit aus, bis entweder ein Tunnelstrom detektiert wird oder 
der Röhrenscanner komplett ausgefahren wird. Wird kein Tunnelkontakt erreicht, wird der Röhrenscanner wieder zurückgefahren und die Probe mit dem Grobantrieb einen Schritt
höher bewegt. Dies wird solange wiederholt bis die Spitze sich im Tunnelkontakt zur Probe befindet.

Damit diese Automatik funktioniert, muss die richtige Spannung zwischen Spitze und Probe angelegt werden, da bei zu geringer Spannung kein Tunnelstrom fließt, die Automatik 
nicht beendet wird und die Spitze in die Probe gerammt wird. Dies muss gerade bei Halbleitern beachtet werden, da hier nur ein Tunnelstrom auftreten kann, 
wenn die Spannung größer ist als die Bandlücke des Halbleiters. 

An einem Oszilloskop kann die Ausgangsspannung, welche durch den Strom-Spannungs-Wandler aus dem Tunnelstrom generiert wird, direkt betrachten werden. Es wird bei einem Tunnelstrom von
\(\SI{100}{\pico \m}\) eine Spannung von \(\SI{100}{\milli \m}\) gemessen. Der Strom-Spannungs-Wandler operiert also mit einem Verstärkungsfaktor \(f = 10^9\frac{V}{A}\) 
wobei \(U = f \cdot I\) gilt.
In diesem Versuch wird eine Spannung von \(\SI{5}{\milli \V}\) zwischen Spitze und Probe angelegt und mit einem Soll-Strom von \(\SI{1}{\nano \A}\) gemessen. 

\subsection{Erste Messung}
Im Tunnelkontakt können dann erste Bilder aufgenommen werden. In diesem Versuch wurden drei Datensätze aufgenommen. Für den Datensatz m110 wurde eine \(\SI{2}{\nano \m} \times \SI{2}{\nano \m}\)
große Fläche vermessen. Für m135 eine \(\SI{5}{\nano \m} \times \SI{5}{\nano \m}\) Fläche und für m136 eine \(\SI{10}{\nano \m} \times \SI{10}{\nano \m}\) Fläche.
Dann werden die Datensätze in das Analyseprogramm Gwyddion geladen (siehe Abbildung \ref{fig:erste_Darstellung}).

\begin{figure}[H]
    \centering
    \begin{subfigure}{.32\linewidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/m110_raw.png}
      \caption{m110}
      \label{sfig:m110_raw}
    \end{subfigure}%
    \begin{subfigure}{.32\textwidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/m135_rawXD.png}
      \caption{m135}
      \label{sfig:m135_raw_firsttime}
    \end{subfigure}
    \begin{subfigure}{.32\textwidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/m136_raw.png}
      \caption{m136}
      \label{fig:m136_raw}
    \end{subfigure}
    \caption{Erste Darstellung der Datensätze, Tunnelparameter: \(U = \SI{5}{\milli \V}, I = \SI{1}{\nano \A}\)}
    \label{fig:erste_Darstellung}
    \end{figure}

Dargestellt wird die Höhe \(z(x,y)\). Zu sehen ist ein Gradient von der linken Seite zur rechten Seite, die Probenöberfläche ist also verkippt gegenüber der Messspitze.
Dieser und weitere Bildfehler werden am Beispiel vom Datensatz m135 korrigiert.

Mithilfe von Gwyddion lassen sich Profile an beliebigen Linien anzeigen (siehe Abbildung \ref{fig:m135_verkippung}). Daran lässt sich dann die Verkippung der Probenoberfläche berechnen.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/m135_Verkippung.png}
    \caption{Profil \(z(s)\) entlang der \(x\)- und \(y\)-Richtung}
    \label{fig:m135_verkippung}
\end{figure}

Für eine grobe Quantifizierung der Verkippung lässt sich mithilfe von Gwyddion eine Gerade an die Profillinien fitten. So ergeben sich für die x-Richtung eine Steigung
von \(b_x = 0.095 \pm 0.87 \cdot 10^{-3}\) und \(b_y = -6.3 \cdot 10^{-3} \pm 0.96\cdot 10^{-3}\) in y-Richtung, dies entspricht dann Winkeln von 
\(\Theta_x = \ang{5.427} \pm \ang{8.6}\cdot 10^{-4} \) und \(\Theta_y = \ang{-0.362} \pm \ang{9.6}\cdot 10^{-4}\) . 

Gwyddion bietet mit der Option "'Level data by mean plane substraction"' die Möglichkeit zur Korrektur verkippter Oberflächen. Wie in Abbildung \ref{fig:m135_plane_flatten} (b)
sichtbar, ist dann kein Gradient mehr zu sehen. Allerdings sind immernoch unterschiedliche Helligkeiten zwischen den Zeilen zu sehen. Diese Unterschiede entstehen durch thermischen 
Drift im Aufbau, wodurch sich Spitze und Probe relativ zueinander bewegen. Um diese Artefakte auszugleichen, gibt es in Gwyddion die Option "'Align rows using various Methods'".
Im Folgenden werden alle Datensätze zuerst mit der Level Data- und dann mit der Align Rows-Funktion bearbeitet.

\begin{figure}[H]
\centering
\begin{subfigure}{.32\linewidth}
  \centering
  \includegraphics[width=\textwidth]{bilder/m135_rawXD.png}
  \caption{Rohdaten}
  \label{sfig:m135_raw}
\end{subfigure}
\begin{subfigure}{.32\textwidth}
  \centering
  \includegraphics[width=\textwidth]{bilder/m135_nurplane.png}
  \caption{Level Data}
  \label{sfig:m135_plane}
\end{subfigure}
\begin{subfigure}{.32\textwidth}
  \centering
  \includegraphics[width=\textwidth]{bilder/m135_Plane_Flatten.png}
  \caption{Plane und align rows}
  \label{sfig:m135_plane_flatten}
\end{subfigure}
\caption{Anwendung von Level Data und Align Rows auf die Rohdaten von m135, Tunnelparameter: \(U = \SI{5}{\milli \V}, I = \SI{1}{\nano \A}\)}
\label{fig:m135_plane_flatten}
\end{figure}

In Abbildung \ref{fig:m135_verkippung_nach_Plane} sind die Profile nach der Korrektur gezeigt. Es lässt sich ein Signalhub von \(\SI{140}{\pico \m} \pm \SI{10}{\pico \m}\)
ablesen. Außerdem ist ein Rauschlevel won wenigen Picometern zu sehen. Ansonsten ist die Probenoberfläche atomar glatt, es sind keine Stufenkanten zu erkennen. 

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{bilder/m135_VerkippungnachPlane.png}
  \caption{Profil \(z(s)\) entlang der \(x\)- und \(y\)-Richtung nach Anwendung der Plane-Funktion}
  \label{fig:m135_verkippung_nach_Plane}
\end{figure}

\subsection{Kalibrierung des Rastertunnelmikroskops an HOPG}
Zur Kalibrierung des RTM wird der m136-Datensatz verwendet, da durch die größere Fläche mehr Daten erhoben werden können.
Außerdem haben Fehler wie Piezo-Creep und thermischen Drift einen geringeren Einfluss auf die Messwerte, da alle Datensätze in der gleichen Zeit aufgenommen wurden und
damit bei größeren Bildern mit einer schnelleren Rastergeschwindigkeit gemessen wird.

\subsubsection{Im Ortsraum}
Die Atome der HOPG-Oberfläche bilden ein Honigwabengitter. Betrachtet man nun das Bild von m136 (Abbildung \ref{fig:m136_normales_Bild}) so sieht man stattdessen ein Dreiecksgitter von hellen Punkten.
Dies liegt an den in Abschnitt \ref{section:ProbeTheo} erklärten unterschiedlichen Elektronendichten der \(\alpha\)- und \(\beta\)-Atome. 
Im Bild sind also nur die \(\beta\)-Atome als helle Punkte zu sehen.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/m136_plane_flatten.png}
    \caption{Bild des Datensatzes m136, Tunnelparameter: \(U = \SI{5}{\milli \V}, I = \SI{1}{\nano \A}\)}
    \label{fig:m136_normales_Bild}
\end{figure}

Der Abstand a zweier benachbarter Atome im HOPG ist mit \(a = \SI{142}{\pico \m}\) bekannt. Daraus ergibt sich ein erwarteter Abstand b zweier \(\beta\)-Atome mit: 

\begin{equation*}
    b = 2\cdot a \cdot \sin(\ang{60}) = 2 \cdot \SI{142}{\pico \m} \cdot \sin(\ang{60}) = \SI{246}{\pico \m}
\end{equation*}

Um das RTM zu kalibrieren wird jetzt der Abstand der hellen Punkte gemessen und mit dem erwarteten Wert verglichen, sodass ein Korrekturfaktor \(f_{corr}\) gefunden werden kann, für den gilt

\begin{equation*}
    L_{real} = f_{corr} \cdot L_{mess}
\end{equation*} 

und die gemessenen Längen in Reale umgerechnet werden können.
Um die Abstände im Datensatz zu messen, werden mit Gwyddion im Bild entlang der drei Symmetrieachsen (Abbildung \ref{fig:m136_periodAchsen}) des Gitters das Profil vermessen.
Diese ist periodisch wobei die Periode gerade der Abstand zwischen den Atomen ist.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/m136_periodizitätachsen.png}
    \caption{Bild des Datensatzes m136, Tunnelparameter: \(U = \SI{5}{\milli \V}, I = \SI{1}{\nano \A}\)}
    \label{fig:m136_periodAchsen}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/m136_Periodizität.png}
    \caption{Bild des Datensatzes m136, Tunnelparameter: \(U = \SI{5}{\milli \V}, I = \SI{1}{\nano \A}\)}
    \label{fig:m136_Periodizitaet}
\end{figure}

Die Profile sind in Abbildung \ref{fig:m136_Periodizitaet} dargestellt. Indem über einen Messbereich d die Anzahl n der Maxima gezählt werden, 
kann mit \(P = \frac{d}{n}\) die Periode P ermittelt werden. Dieses Vorgehen verringert die statistischen Fehler beim Messen der Abstände.
Es ergibt sich für die Perioden der Profile: 

\begin{table}[H]
    \centering
    \begin{tabular}{  c c c c }
    \hline
                & Messbereich (in \(\si{\nano\m}\))  & Anzahl Maxima & Periode (in \(\si{\nano\m}\)) \\\hline
       Profil 1 & \(7.95 \pm 0.1\)                       & 36            & \(0.221 \pm 0.0027\)              \\\hline
       Profil 2 & \(7.68 \pm 0.1\)                       & 27            & \(0.284 \pm 0.0037\)              \\\hline
       Profil 3 & \(7.68 \pm 0.1\)                       & 27            & \(0.284 \pm 0.0037\)              \\\hline
    \end{tabular}
    \caption{Perioden der drei Symmetrieachsen}
    \label{tab:auswertung_ortsraum}
\end{table}

Es wird dabei für den Messbereich ein Ablesefehler von \(\SI{0.1}{\nano\m}\) angenommen.
Wie man sieht, sind nicht für alle Symmetrieachsen die Atomabstände gleich. Dies liegt daran, dass Fehler wie thermischer Drift oder Piezo-Creep die Aufnahme über längere Zeit verzerren.
Da die Zeilen in x-Richtung im Vergleich zur y-Richtung sehr schnell abgefahren werden, kommt es innerhalb einer Zeile nur zu einer geringen Verzerrung.
Dementsprechend ist auch der Atomabstand auf der Symmetrieachse, die am parallelsten zur x-Richtung liegt, am wenigsten verändert und damit am nächsten am reellen Wert.
Deshalb wird zur Berechnung des Korrekturfaktors der Atomabstand entlang der Symmetrieachse 3 verwendet. Daraus folgt für den Korrekturfaktor:
\begin{equation*}
    f_{corr} = \frac{L_{real}}{L_{mess}} = \frac{\SI{246}{\pico \m}}{\SI{284}{\pico \m}} = 0.866 \pm 0.011
\end{equation*}  

\subsubsection{Im reziproken Raum}
Eine alternative Möglichkeit zur Auswertung der Daten ist die Fourier-Transformation der Daten, also die Auswertung im reziproken Raum. Ein hexagonales Gitter ist die Überlagerung
aus drei sinusförmigen Wellen: eine in x-Richtung, eine in y-Richtung und eine diagonale, sodass sich 3 Fourierkomponenten (mit entsprechenden Spiegelungen) ergeben 
(siehe Abbildung \ref{fig:fourier_skript}).
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/fourier_skript.png}
    \caption{Hexagonales Gitter im Realraum und im reziproken Raum \cite{krause_2020}}
    \label{fig:fourier_skript}
\end{figure}
Die Länge der Fourierkomponenten ergibt sich damit aus dem Abstand \(\lambda\) der Geraden, die durch beispielsweise die \(\beta\)-Atome gebildet werden. 
Dies ist die Höhe des durch den Abstand \(b\) der \(\beta\)-Atome aufgespannten gleichseitigen Dreiecks: 
\(\lambda = \frac{\sqrt{3}}{2} \cdot b = \frac{\sqrt{3}}{2} \cdot \SI{0.246}{\nano\m} = \SI{0.213}{\nano\m}\).

Für die Analyse der Fourierkomponenten wird genau wie im Ortsraum der m136-Datensatz genutzt. In Abbildung \ref{fig:m136_fourier} ist das gesamte 
Fourierspektrum des Datensatzes m136 dargestellt, in Abbildung \ref{fig:m136_fourier_klein} ist der relevante Ausschnitt um den Nullpunkt ausgeschnitten. 
In Abbildung \ref{fig:m136_fourier} sind neben den Fourier-Spots ein Rauschen über den gesamten Wertebereich
zu erkennen. In Abbildung \ref{fig:m136_fourier_klein} ist das erwartete Hexagon-Muster zu erkennen. Dieses ist, genau wie die Messdaten selber ein wenig verdreht.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{bilder/m136_Fourier.png}
    \caption{Fouriertransformation von m136}
    \label{fig:m136_fourier}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/m136_fourier_klein.png}
    \caption{Ausschnitt aus der Fouriertransformation von m136}
    \label{fig:m136_fourier_klein}
\end{figure}

Gwyddion bietet die Möglichkeit, die Fourierkomponenten selektiv in den Ortsraum rück-zutransformieren. Werden die 6 Fourierkomponenten 1. Ordnung, wie in Abbildung 
\ref{fig:m136_fourier_klein} rücktransformiert ergibt sich das Bild in Abbildung \ref{fig:m136_fourier_invers}.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/m136_fourier_invers.png}
    \caption{m136 nach Filterung im reziproken Raum}
    \label{fig:m136_fourier_invers}
\end{figure}

Zum Vermessen der Abstände der Abstände wird wieder das Profillinien-Tool von Gwyddion genutzt (siehe Abbildung \ref{fig:m136_fourier_messung}, mit den vermessenen
Achsen in Abbildung \ref{fig:m136_fourier_messung_achsen}). Wichtig ist zu beachten, dass Gwyddion im reziproken Raum nicht \(k = \frac{2\pi}{\lambda}\) angibt, sondern
\(k = \frac{1}{\lambda}\). Es ergibt sich also, wenn der Abstand \(d\) zwischen zwei gespiegelten Fourierkomponenten gemessen wird:
\begin{equation}
    \lambda = \frac{1}{\frac{d}{2}} = \frac{2}{d}
\end{equation}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/m136_fourier_Messung.png}
    \caption{Messung der Abstände zwischen den Fourierkomponenten}
    \label{fig:m136_fourier_messung}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{bilder/m136_fourier_Messungsachsen.png}
    \caption{Achsen an denen die Profile entnommen wurden}
    \label{fig:m136_fourier_messung_achsen}
\end{figure}

\begin{table}[H]
    \centering
    \begin{tabular}{  c c c }
    \hline
                & Abstand Fourierkomponenten (in \(\si{\nano\m}^{-1}\))  & Wellenlänge \(\lambda\) (in \(\si{\nano\m}\)) \\\hline
       Profil 1 & \(9.94 \pm 0.1\)                         & \(0.201 \pm 0.0020\)  \\\hline
       Profil 2 & \(7.68 \pm 0.1\)                         & \(0.260 \pm 0.0033\) \\\hline
       Profil 3 & \(9.82 \pm 0.1\)                         & \(0.204 \pm 0.0020\) \\\hline
    \end{tabular}
    \caption{Abstände der Fourierkomponenten und resultierende Wellenlänge}
    \label{tab:auswertung_reziprok}
\end{table}

Genau wie im Ortsraum sind die Verzerrungen des Bildes im reziproken Raum richtungsabhängig. Die gemessenen Achsen im reziproken Raum entsprechen allerdings nicht
genau den gemessenen Achsen im Realraum, da die Länge der Fourier-Komponenten der reziproke Abstand zwischen den Atomreihen (z.B. der \(\beta\)-Atome) ist, die
Achsen sind also gegenüber den Messachsen im Realraum um \(\ang{30}\) verschoben. Die bereits genannten Effekte wie der thermische Drift haben auch im reziproken
Raum eine geringere Auswirkung, je kleiner der Winkel zur x-Achse ist. Deshalb wird für die Berechnung des Korrekturfaktors das Profil 2 genutzt:
\begin{equation*}
    f_{corr} = \frac{\lambda_{real}}{L_{mess}} = \frac{\SI{0.213}{\nano \m}}{\SI{0.260}{\nano \m}} = 0.819 \pm 0.010
\end{equation*}  


\section{Diskussion}
Die Messung wird durch verschiedene Fehler beeinflusst.

\subsection{Rauschen}
Die Nadel wird mit piezoelektrischen Bauteilen bewegt. Diese werden mit einer Spannung von bis zu \(\SI{100}{\V}\) betrieben. Die Hochspannungsverstärker, die diese 
Spannung liefern, haben ein Rauschen von ungefähr \(\SI{1}{\milli \V}\). Dies führt zu einem Wackeln der Spitze um wenige Picometer, was sich als Rauschen im Bild wiederspiegelt.

\subsection{Verkippung}
Aufgrund der Präparation von Spitze und Probe ist es nicht möglich, vollkommen senkrecht mit der Spitze auf der Probe zu messen. Dadurch muss die Spitze weiterausgefahren
werden um die von der Sonde weggekippten Seite zu vermessen, was zu einem Farbgradient im Bild führt. 

\subsection{Thermischer Drift}
Das RTM besteht aus vielen verschiedenen Materialien, welche alle verschiedene Wärme-ausdehnungskoeffizienten haben. 
Dadurch sind bei konstanter Steuerspannung, die Position der Probe und die Tunnelspitze relativ zueinander nie komplett fixiert. 
Durch kleine Temperaturschwankungen in der Umgebung des RTM, kann es im Laufe einer Messung passieren, dass sich 
die Probe unter der Spitze verschiebt. Dies führt zu Helligkeitsunterschieden entlang der y-Richtung. 

\subsection{Piezo-Creep}
Wird die Spannung an einem piezoelektrisches Element schlagartig umpolarisiert, kann das Element seine Auslenkung nicht sofort vollständig umkehren, es driftet nach 
der Änderung noch eine kleine Strecke weiter in die Richtung der Spannung.
Genau dies passiert, wenn nach einer Messung die Spitze wieder zurück in die Anfangsposition gestellt wird. Dies führt dazu, dass zu Beginn der nächsten Messung,
die Spitze zwar in y-Richtung bewegt wird, die Piezoelemente aber noch in die entgegengesetzte Richtung driften und sich die beiden Bewegungen überlagern. 
Dadurch kommt es gerade am unteren Rand des Bildes zu Verzerrungen.

\subsection{Fehlerkorrektur}
Die Fehler durch die Verkippung und den thermischen Drift können durch die Tools von Gwyddion ausgeglichen werden.
Ein zu großer Einfluss des Piezo-Creeps kann dadurch verhindert werden, indem die verzerrten Ränder des Bildes nicht in die Messung miteinbezogen werden.
Restliche Fehler wie z.B. das Rauschen können zum Einen durch eine Vergrößerung des Messbereiches vermindert werden. Außerdem können die Messzeilen entlang einer der Symmetrierichtungen 
ausgerichtet werden, was auch den Einfluss von Fehlern wie den thermischen Drift und Piezo-Creep verringert. 
Allgmein hätten noch mehr Messungen durchgeführt werden müssen um noch genauere Werte zu finden.

\section{Zusammenfassung}
In dem Versuch haben wir die Funktionsweise eines Rastertunnelmikroskops kennengelernt. Dies umfasste die Präparation von Probe
und Spitze, das Kennenlernen der Analysesoftware Gwyddion und schließlich das Auswerten von Messungen an HOPG, anhand derer wir dann
die gemessenen Gitterkonstanten und Länge der Fourierkomponenten mit den theoretischen Werten von HOPG verglichen haben. 

Die Kalibrierung im Ortsraum ergab einen Korrekturfaktor von \(f_{corr} = 0.866 \pm 0.011\).  Die Kalibrierung im reziproken Raum ergab 
\(f_{corr} = 0.819 \pm 0.010\). Diese Werte sind nah beieinander, weichen aber immernoch leicht voneinander ab. Eine größere Menge an Messungen könnte 
einen genaueren Wert liefern.


\newpage
\thispagestyle{empty}
\bibliography{protokoll_kv3}
\bibliographystyle{abbrv}
\addcontentsline{toc}{section}{Literatur}



\end{document}