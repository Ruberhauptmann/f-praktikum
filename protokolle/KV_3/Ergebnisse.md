Image Export: Outline Thickness 0.16

# Erste Messung
## Kippung

Betrachte Profil entlang x- und y-Richtung, Fit von Gwyddion mit Gerade
(im Text: Profilfunktion erklären, z(s), was ist s)
Für x-Richtung: Steigung b = 0.095 +- 0.87*10^-3 -> 5.43°
Für y-Richtung: Steigung b = -6.3*10-3 +- 0.96*10-3 -> -0.36°

## Korrugation
Signalhub: 187.6 pm
Rauschlevel: klein gegenüber Signalhub, wenige pm

## Terassen
Erwarten: keine Terassen, sehen keine Terassen

# Kalibrierung
##  1
Erwartung: Dreiecksgitter

Realität: Ausgewaschenes Dreiecksmuster

## 2
beta Atome hell

## 3
erwartung: b = 2*sin(60°)*a = 0.246nm

## 4
Fehler anhand von 135 erklären, Vergleich mit 136 zur Visualisierung

## 5
Messung an 136
Profile an arbiträren Linien
3 Symmetrierichtungen

Messungen von mehreren Tälern für Periodizität -> Ablesefehler mitteln sich raus

Fehler: Ablesefehler/Messgenauigkeit von Gwyddion
Rauschen: 1pm, Größenordung kleiner, also vernachlässigbar

(Achtung, 0.01nm ist für die Längenmessung -> Fehlerfortpflanzung!)
- Profil 1: 7.95 / 36 = 0.221 +- 0.01 nm
- Profil 2: 7.68 / 27 = 0.284 +- 0.01 nm
- Profil 3: 7.68 / 27 = 0.284 +- 0.01 nm

## Korrekturfaktor
Wähle Messung Profil 3, da dies am nächsten an der Waagrechten ist -> wenigster Einfluss von thermischen Drift

Fehlerfortpflanzung?

f_corr = 0.246/0.28 = 0.89

# Fourier
## 1
Abbildung aus Skript
Berechne Länge der Fourierkomponenten

## 2
136: Siehe Ortsraum

## 3
Einmal Rohbild, einmal zugeschnitten

Gutes Hexagon, höhere Ordnungen, Rauschen, Punkte sind nach oben und unten verschmiert

## 4
Wichtig: Werte in Gwyddion sind 1/\lambda, nicht in k = 2pi/\lambda

Erwartung: Gleichseitiges Dreieck, \lambda = \sqrt(3)/2 * b = 0.213nm
Abstand d, d/2 eine Fourierkomponente, \lambda = 1/(d/2) = 2/d

Ablesefehler: 0.01 nm^-1 (Fehlerfortpflanzung)
- Profil 1: 1/\lambda/2 = 9.94 nm^-1, \lambda = 0.201 nm
- Profil 2: " = 7.68nm^-1, \lambda = 0.260 nm
- Profil 3: " = 9.82 nm^-1, \lambda = 0.204 nm

Wähle 2 (siehe Ortraum)
f_corr = 0.82

# Fehlerfortpflanzungsversuch
s_G = sqrt(sum((dG/dx)^2*s_x^2)) , Ableitung am Mittelwert von x
Deshalb auch immer Werte von allen drei achsen benutzt. So kamen auch sinnvollere werte raus

## Kalibrierung
Angenommener Ablesefehler: 0.1nm
s_L = 0.1nm / n
n \in [36,27,27]

s_f = L_real / L_mess^2 * s_L_mess = 0.011

## Fourier
Angenommener Ablesefehler: 0.1nm^-1
s_l = 2/d^2 * 0.1nm^-1

s_f = lambda_real / lambda_mess^2 * s_lambda_mess =

# Tunnelparameter
U = 5 mV
I = 1 nA
