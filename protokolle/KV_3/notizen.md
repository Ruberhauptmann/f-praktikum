# Fragen für Donnerstag
- Was genau heißt Signalhub/Rauigkeit?
- Abstract, toc -> Ist geklärt, war ein Fehler in einem \addcontensline Befehl
- Fouriererwartung
- 80/20 Draht!
- Literatur?
- wann die fragen beantworten?

# Präparation einer neuer Tunnelspitze

Spitze reißen, in Sacklochbohrung einsetzen (auf Spannung)

Frage: 

1. Warum ist es wichtig, eine neue Spitze zu präparieren? Bedenken Sie, was physisch und chemisch passiert, wenn die Spitze über längere Zeit an Luft belassen wird, oder was ggf. bei vorangegangenen Experimenten beabsichtigt oder unbeabsichtigt passiert sein könnte.
- Anlagerung von Fremdatomen (z.B. aus der Luft) kann verschiedene Konsequenzen haben: Bildung von mehreren Spitzen, Verbreiterung der Spitze, s.d. nicht mehr feine Strukturen abgebildet werden können
- Ein Zusammenstoß mit der Probe kann die Spitze "flach machen" oder zur Ausbildung weiterer Spitzen führen

# Präparation einer neuen Probe

Probe befindet sich (leitend befestigt) auf Probenträger, Präparation: leichtes Andrücken eines Klebesteifens, abziehen -> glatte Oberfläche mit möglichst wenig Flocken

Fragen:

1. Warum zieht man mit dieser Methode nicht ungeordnet einzelne „Brocken“ aus der Probe,sondern entfernt meist glatte, sehr dünne Filme? Diskutieren Sie die verschiedenen Bindungen, die in der Probe vorliegen, und erklären Sie, warum das Abziehen mit einem Klebestreifen besonders gut für die Präparation geeignet ist.
- Innerhalb der Schichten werden \sigma-Bindungen zwischen den Kohlenstoffatomen ausgebildet, zwischen den Schichten bildet das p_z-Orbital nur \pi-Bindungen aus, die viel schwächer sind als die \sigma-Bindungen. Damit werden beim (vorsichtigen) Abziehen mithilfe des Filmstreifens die Bindungen zwischen den Schichten gelöst und nicht innerhalb der Schichten

2.   Warum muss überhaupt eine neue Oberfläche präpariert werden? Überlegen Sie, was mit einer atomar reinen Oberfläche (physisch und chemisch) passiert, wenn sie über längere Zeit unter Umgebungsbedingungen (an Luft) gelagert wird. Warum werden Experimente an besonders reinen Oberflächen vorzugsweise unter Ultrahochvakuumbedingungen durchgeführt?
- Es lagern sich Fremdatome an der Oberfläche ab -> deutlich mehr Rauschen in Aufnahmen, evtl. dann gar kein Rückschluss auf periodische Strukturen möglich, da nur oberste Schichten gemessen werden, makroskopische Beschädigungen (z.B. Kratzer durch Zusammenstoß mit der Messspitze)
- Probe lässt sich mehrere Male messen, Fremdatome sind bei besonders reinen Oberflächen besonders schädlich für Messungen

# Annäherung an den Tunnelkontakt

Grobantrieb: "slip" und "stick" Prinzip: Probe ist auf Saphirprisma, wird von Piezo-Scheraktoren gehalten. Beim langsamen Anlegen einer Spannung an Piezos: Scheren aus, bewegen das Prisma. Bei schnellen Hoch/Runterfahren der Spannung: Schnelle Bewegung der Piezos, Prisma kann nicht folgen (Piezos rutschen an der Saphiroberfläche in Ausgangsposition

Fragen:

1. Was passiert, wenn die Sägezahnspannung nicht optimal auf den Antrieb angepasst ist? Diskutieren Sie einen flachen bzw. steilen Anstieg der angelegten Spannung im „stick“-Bereich und einen langsamen bzw. schnellen Abfall der Spannung im „slip“-Bereich. Kann die Frequenz und Spannung beliebig erhöht werden, um die effektive Geschwindigkeit beliebig zu erhöhen?
- Damit die Aktoren das Prisma bewegen und nicht rutschen, muss die Spannung langsam steigen. Bei einem schnellen würden die Aktoren ausscheren, allerdings würde das Prisma der Bewegung nicht folgen, es würde also zu keiner Bewegung kommen. Bei einem langsamen Abfall im slip-Bereich würde das Prisma dem Aktoren folgen, also die Bewegung aus dem slip-Bereich wieder rückgängig machen, wohingegen bei einem schnellen Abfall das gewünschte Rutschen am Prisma eintritt.
- Damit kann die Frequenz nicht beliebig erhöht werden, sie muss angepasst werden auf die Eigenschaften der Aktoren, d.h. bei welchem Anstieg/Abfall die Aktoren am Prisma halten bzw. rutschen.

Feinantrieb: Automatik, fährt die Piezoröhre aus, misst Tunnelstrom -> Wird am Oszilloskop gemessen (Ausgangsspannung, die der Strom-Spannungs-Wandler aus dem gemessenen Tunnelstrom generiert)
Soll-Strom: 100 pA -> Spannung 100 mV (Mess-Signal des Tunnelstroms)

Fragen:

1. Welche Spannung sollte man bei der Annäherung zwischen Spitze und Probe anlegen? Bedenken Sie die Tunnelbedingungen: Elektronen tunneln elastisch (also ohne ihre Energie zu verändern) von besetzten Zuständen der einen Seite des Tunnelkontakt in unbesetzte Zustände der anderen Seite. Was ist daher insbesondere bei Halbleitern zu beachten (Stichwort: Bandlücke)? Was würde bei Nutzung der oben beschriebenen Annäherungsautomatik passieren, wenn man eine Spannung anlegt, bei der die Tunnelbedingungen nicht erfüllt werden können, egal wie nahe sich Spitze und Probe kommen? 
- evtl. Bandlücken
- Bei Halbleitern muss insbesondere ein Spannung größer als die Bandlücke (E = e*U) angelegt werden, da ansonsten keine Elektronen tunneln können
- Wenn bei der Annäherung zu keiner Zeit ein Tunnelstrom fließt, wird die Automatik weiterlaufen, bis die Spitze die Probe berührt

2. Welchen Verstärkungsfaktor f hat der Strom-Spannungswandler, wenn die ausgegebene Spannung U = f \* I ist (I ist der Tunnelstrom)? Beachten Sie die richtigen Einheiten von f!
- \(f =  1E9 V/A\)

# Erste Messung

Erstes Bild: Tunnelspitze läuft zeilenweise über die Probe ("schnelle Richtung": x, "langsame Richtung": y), es wird z so geregelt, dass der festgelegte Solltunnelstrom I fließt

Fragen:

1. Laden Sie den Datensatz (im folgenden „Bild“ genannt) in das Analyseprogramm WSxM, ohne die Kästchen „plane“ und „flatten“ im „Datei öffnen“-Dialog zu aktivieren. Was sehen Sie? Ist die Probenoberfläche verkippt? Woran kann dies liegen?
- Helle und dunkle periodische Struktur, horziontale Linien (Rauschen), Oben rechts heller, unten links dunkler -> ist um x- und y-Achse verkippt
- Probenhalter liegt nicht eben, Kristall ist nicht eben befestigt

2. Berechnen Sie den Winkel der Verkippung gegenüber der Horizontalen, einmal in x- und einmal in y-Richtung und quantifizieren Sie so die Verkippung der Probe im Raum.
- -> Höhenprofil (Extract Profile) entlang der Diagonale, berechne Winkel zwischen den Geraden

3. Aktivieren Sie nun nacheinander die Optionen „plane“ und „flatten“ und laden die Daten erneut. Was ist nun anders? Welche Aufgaben haben die „plane“- und „flatten“-Filter?
- Plane: Fittet eine Ebene und zieht diese von den Messdaten ab -> Verkippung wird ausgeglichen
- Flatten: Gleicht den Mittelwert aller Zeilen aneinander an (nimmt Stufen raus)

4. Welche Korrugation (z.B. Rauigkeit, Signalhub oder Rauschlevel) beobachten Sie auf der so korrigierten Oberfläche?
- Signalhub: ??? Was bedeutet exakt (allgemeiner Hub des ganzen Signals, Höhen werden höher, Tiefen tiefer?)
- Rauigkeit: ??? Statistical Function, Heigth Distribution? -> Ein Peak, d.h. keine Terassen
- Rauschlevel: Existiert, sichtbar sowohl im Bild als auch in Height distribution

5. Vergleichen Sie die gemessenen Werte der Korrugation mit Stufenhöhen, die Sie aufgrund der Gitterparameter der Probe erwarten würden. Ist die Probenoberfläche atomar glatt, oder beobachten Sie atomare Stufenkanten bzw. Terrassen?
- Von der Probe (HOPG) erwarten wir keine Terassen (siehe Punkt 2, Vorbereitung der Probe), deshalb ist es gut, dass wir auch keine sehen

# Kalibrierung des Rastertunnelmikroskops an HOPG

RTM misst keine Längen, sondern die angelegten Spannung U_x, U_y am Röhrenscanner (genauso z-Richtung) -> Kalibrierung, nutze dafür bekanntes Material, z.B: HOPG. Bestimme Korrekturfaktor, s.d. l_real = f_corr \* L_Mess

Fragen:

1. Laden Sie die HOPG-Datensätze in das Analyseprogramm. Wenn nötig, aktivieren Sie den „plane“- bzw. „flatten“-Filter. Was würden Sie erwarten zu sehen? Was sehen Sie tatsächlich?
- Hexagon-Muster (Struktur von HOPG), sehen: Rautenmuster

2. Beachten Sie, dass nicht alle Oberflächenatome auf HOPG gleichwertig sind. Es gibt alpha- und beta- Atome (s.o.). Auf welchem Gitter liegen sie dem Rastertunnelbild als helle Erhebungen sichtbaren Atome?
- beta-Atome auf den hellen Erhebungen

3. Welchen Abstand erwarten Sie für diese Atome untereinander, errechnet aus den bekannten Gitterparametern?
- Latice: Bei kleinsten und mittleren: Asymetrisch, bei größten symmetrisch (warum?), Längen ausmessen und vergleichen mit Abständen zwischen den betas (aus Gitterparameter)

4. Entsprechen die RTM-Bilder Ihren Erwartungen bzgl. der Symmetrieeigenschaften des perfekten HOPG-Gitters? Falls nicht: Diskutieren Sie verschiedene Effekte, die zu Artefakten in der Rastertunnelmikroskopie haben können: Thermischer Drift, Piezo-Creep, Rastergeschwindigkeit, ....
- Beim Größten ja: Gittervektoren sind gleich lang, bei den anderen beiden eher nicht
- Artefakte: Research

5. Erstellen Sie ein Linienprofil entlang einer atomaren Reihe, um den Abstand zwischen den hellen Atomen (Periodizität) zu bestimmen. Welcher Datensatz eignet sich hierfür am besten? Überlegen Sie, wie Sie für die Analyse der Daten am besten vorgehen: Direkt den Abstand zweier Atome messen, oder die Länge einer Kette von ca. zehn Atomen messen und durch die Kettenlänge teilen? Welche der beiden Methoden führt zu einem geringeren Ablesefehler und ist daher die genauere Methode?
- Der Größte
- Profil analysieren: Periodizität Graphen z(x) -> Länge von 10, dann teilen n(-> geringerer Ablesefehler)

6. Vergleichen Sie den gemessenen Abstand benachbarter heller Punkte mit dem erwarteten Abstand z.B. der alpha-Atome und ermitteln so den Korrekturfaktor fcorr für die Kalibrierung.
- Fehler von RTM aus: 1pm

7. Analysieren Sie atomare Reihen entlang der drei beobachteten Richtungen hoher Symmetrie. Sind die gemessenen Abstände der Atome immer die gleichen, egal in welcher Richtung die Daten analysiert werden? Entspricht dies den Erwartungen für ein HOPG-Gitter?
- Messen: tbd, irgendwie mit Höhenprofilen entlang den Richtungen
- Erwartung: sollte in alle drei Richtungen der gleiche Abstand sein

8. Finden Sie mögliche Erklärungen für Abweichungen der Kalibrierungen entlang der unterschiedlichen Richtungen. Stichworte: Piezo-Creep, Thermischer Drift, ....
- Richtungsabhängige Fehler beeinflussen Symmetrieachsen unterschiedlich

Möglichkeit zur Analyse der Gesamtheit aller Daten -> Fourier-Transformation, Analyse der periodischen Anordnung von Atomen

Fragen:

1. Wenn Sie eine „perfekte“ RTM-Abbildung des HOPG-Gitters einer Fourier-Transformation unterziehen würden, was würden Sie als Ergebnis erwarten? Diskutieren Sie Richtungen und Längen der erwarteten Fourier-Komponenten, und beachten Sie, dass das beobachtete Gitter nur alpha- bzw. beta-Atomen des HOPG-Gitters besteht.
- Erwartung: Rauten im reziproken Raum (periodische Struktur im Ortsraum ist Raute) bzw. scharf definierte Punkte, die ein Rautenmuster bilden

2. Laden Sie einen geeigneten Datensatz in das Datenanalyseprogramm. Welcher Datensatz erscheint Ihnen am geeignetsten für eine Fourier-Analyse?
- Der Größte (Rauschen am wenigsten Einfluss, da über mehr Punkte gemittelt werden kann)

3. Führen Sie eine Fourier-Transformation der Daten in den reziproken Raum durch. Was beobachten Sie? Entspricht die Beobachtung der Erwartung?
- Punkte (sternförmig), die ein Rautenmuster bilden

4. Analysieren Sie die Fourier-Spots, die zum beobachteten Gitter im Real-Raum gehören, indem Sie die Länge der entsprechenden k-Vektoren bestimmen. Welche Richtungen im Realraum werden den k-Vektoren im reziproken Raum zugeordnet?
- 

5. Führen Sie eine selektive Rücktransformation der Fourier-Spots in den Realraum durch. Entspricht das so erhaltene Gitter dem Gitter, das Sie in den Rohdaten des RTM-Bildes sehen? Beachten Sie, dass Sie durch eine geeignete Wahl der Fourier-Komponenten im reziproken Raum jedes beliebige Gitter im Realraum konstruieren können, das gegebenenfalls nichts mehr mit dem Orignalbild zu tun hat!
- 

6. Berechnen Sie aus den k-Vektoren die entsprechenden Abstände z.B. der -Atome im Realraum und kalibrieren hiermit das Rastertunnelmikroskop auf eine zweite Art (zusätzlich zur Kalibrierung im Realraum oben).
- 

7. Sehen Sie wieder Richtungsabhängigkeiten des Fourier-Spektrums? Falls ja: Korrespondieren diese zu den beobachteten Richtungsabhängigkeiten im Realraum?
- 

8. Diskutieren und begründen Sie, entlang welcher Richtung die Daten für die Kalibrierung vorzugsweise analysiert werden sollten, um unerwünschte Verzerrungseffekte (s.o.) während der Datenaufnahme zu minimieren.
- Irgendwie abhängig, wie stark gekippt im Vergleich zu horizontalen Linien (Frage: eher parallel oder eher senkrecht (mitteln über alle Zeilen?))

9. Vergleichen Sie die im Real- und im reziproken Raum erhaltenen Kalibrierungen. Stimmen sie überein, und wo sehen Sie Abweichungen?