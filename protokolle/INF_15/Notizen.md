## Fragen an Pirmin
Wie sollen mit den statistischen Größen von Gwyddion umgehen -> Fehler?

Gwyddion

immer plane, flatten

Statistical functions, Measure und Terraces

Export: pdf!

## Pirmin06 und Pirmin02
Beide roh exportiert (ohne Bearbeitung)
-> Probe insgesamt verkippt, sehen trotzdem Facetten

Plane: Ebenenfit über das gesamte Bild, trotzdem Helligkeitsunterschiede zwischen den Zeilen

Row: (media) Nimmt das auch raus -> prominente Features sind jetzt die Facetten

(Ein Bild nach beidem)

Graph: Höhenprofil senkrecht zu den Facetten -> evtl. Vergleich mit theoretischen (idealen) Skizzen (Spitze)

### Auswertung
#### P02
1. Statistische Werte (gesamtes Bild):
r_rms = 152.8 pm
r_a = 119.6 pm
Theta = 0.02° -> Verkippung des Gesamtplanes
Phi = 19.90° ?

2. HHCF
sigma = 164.60 pm +- 0.72 pm
T = 51.613 nm +- 1.3 nm

Graphen exportiert

3. Höhenverteilung
Gauß-Verteilung, mit Facetten (zwei symmetrische Peaks), sigma = 209.91 pm +- 3.6 pm

4. Polynomfit (1. Grades) an eine Facette zum Ausgleich der Verkippung
-> Rauigkeit innerhalb einer Facette
r_rms = 103.8 pm
r_a = 81.3

Dann nochmal Analyse mit HHCF:
sigma = 98.189 pm +- 1.3 pm
T = 10.171 nm +- 1.9 nm

Höhenvert.: sigma = 139.56 pm +- 0.49 pm

5. Measure Facets zur Messung der Verkippung der Facetten
Erstmal "Hauptverkippung"
Theta =  0.064 °
Phi =  -25.954 °

Für Länge: zwei Möglichkeiten (im Ortsraum Auszählen zwischen den Spitzen und Fourieranalyse)
Ortsraum: Messe Länge der Facette: 255,2 nm
 -> Fehlerfortpflanzung mit Ablesefehler 0.01 mikrometer)
Gitterkonstante:  255.2 nm * tan(0.064) = 286,18 pm

Fourier: Messen Abstand d zwischen Hauptkomponenten: d = 7.6 mikrometer^-1
Gitterkonstante: 1/7.6/2 mikrometer * tan(0.064) = 293.95 pm

6. Facet
372.2 +- 0.8 pm

#### P06
1. Statistischen Werte (gesamtes Bild)
r_rms = 138.5 pm
r_a = 112.3 pm
Theta = 0 °
Phi = 7.51 ° ?

2. Statistische Fkt. (HHCF) (gesamtes Bild)
Gauß:
sigma = 139.92 pm +- 0.52 pm = r_rms
T = 27.914 nm +- 1.8 nm

Graphen exportiert (Normale Skala), mit Fit, h (in m^2) gegen t (in m)

3. Höhenverteilung
-> Gauß-Verteilung! sigma = 207.42 pm +- 0.92 pm
Graphen exportiert

4. Polynomfit (1. Grades) an eine Facette zum Ausgleich der Verkippung
-> Rauigkeit innerhalb einer Facette
r_rms = 95.37 pm
r_a = 73.97 pm -> r_rms merkt z.B. Überschneiden der Maske mit anderen Facetten mehr

Dann nochmal Analyse mit HHCF: sigma = 95.821 pm +- 0.79 pm, T = 7.5006 nm +- 0.72nm (!divergenz nicht mit gefittet!); Höhenvert.: sigma = 125.73 +- 0.56 pm

5. Measure Facets zur Messung der Verkippung der Facetten
Erstmal "Hauptverkippung"
Theta = 0.086 °
Phi = -21.030 °

Für Länge: zwei Möglichkeiten (im Ortsraum Auszählen zwischen den Spitzen und Fourieranalyse)
Ortsraum: Messe Abstand von 8 Spitzen:
1.814 mikrometer / 8 -> Fehlerfortpflanzung mit Ablesefehler 0.01 mikrometer)
Gitterkonstante: 1.814 mikrom / 8 * tan(0.086) = 340.35 pm

Fourier: Messen Abstand d zwischen Hauptkomponenten: d = 8.8 mikrometer^-1
Gitterkonstante: 1/4.4 mikrometer * tan(0.086) = 341.13 pm

6.  Tool Fit Facet
-> fittet Terrassen an die Daten, kann dann Höhe (Gitterkonstante) ausgeben
-> 374.8 +- 0.7 pm

### Auswertung eigene Bilder
#### E20
Profil über Kante, einzelne Fits für beide Stufen:

Höhenunterschied: a = 398 pm +- 10 pm

1. Statistische Werte gesamt Bild:
- E20: r_rms = 266.3 pm, r_a = 207.0 pm, theta = 0.01°, Phi = -177.36°
- E18: r_rms = 341.5 pm, r_a = 227.7 pm, theta = 0.00°, Phi = -120.83°

2. Statistische Funktionen:
- E20: HHCF: sigma = 268.88 pm +- 0.28 pm, T = 7.6692 nm +- 0.13 nm ; Höhenverteilung: sigma = 312.36pm +- 3.5 pm
- E18: HHCF: sigma = 342.16 pm +- 0.33 pm, T = 10.308 nm +- 0.28 nm ; Höhenverteilung: sigma = 328.43 pm +- 3.8 pm

3. gaußglocke in 2. done (höhenverteilung)

4. Polynomfit (1. Grades) an eine Facette zum Ausgleich der Verkippung
- E20: r_rms = 240.6 pm, r_a = 185.2 pm; HHCF: sigma = 238.04 pm +- 0.28 pm, T = 5.9904nm +- 0.11 nm, (!absturz am ende nicht mitgefittet!); Höhenvert.: sigma = 272.66+-3.2pm
- E18: r_rms = 270.5 pm, r_a = 204.6 pm; HHCF: sigma = 267.13 pm +- 0.75 pm, T = 6.0513nm +- 0.28 nm, Höhenvert.: sigma = 276.84 pm +- 5.3 pm

5. Measure Facets zur Messung der Verkippung der Facetten
- Nicht möglich durch partikel.
-Fourier:
- hübsche Bilder, Periodizität im zuschnitt erkennbar

6.  Tool Fit Facet
- E18: step Height: 346.5 pm +- 1.1 pm nicht verlässlich
- E20: stepheight: 361.2 pm +- 1.1 pm siht nett aus

# Auswertung XRR
Daten aus dem Fit:
Sigma:  9.635378006728496 +- 0.06414964464803397
Offset C:  240.78173074183167 +- 37.42568374592152
Norm N:  4.58520456507086 +- 0.024928324427634947
Covariance matrix:  [[4.11517691e-03 5.66756615e-01 1.54628252e-03]
 [5.66756615e-01 1.40068180e+03 1.69852235e-01]
 [1.54628252e-03 1.69852235e-01 6.21421359e-04]]


Footprint-Correction:

Kritischer Winkel (ab dem trifft das ganze Licht auf die Probe)
Mit Probenlänge L = 10mm, Laserbeam mit Breite b = 0.4mm
\theta_b = arcsin(b/L) = arcsin(0.4mm/10mm) = 2.29°

Intensity reduction:
I = beta I_0

mit beta = sin(theta_i / theta_b)

Sigma:  1.6305941697448403 +- 0.06646296316937914
Offset C:  11.63897405202059 +- 0.5859239898689034
Norm N:  6.975010755839273 +- 0.22528690264411733