"""

"""


import matplotlib.pyplot as plt
import numpy as np
import matplotlib
from scipy.optimize import curve_fit
matplotlib.rcParams.update({'font.size': 16})

# Konstanten
## Wellenlänge des genutzten Lasers (in Angström)
LASER_WAVELENGTH = 0.71
## Breite des Lasers und Länge der Probe (in mm)
LASER_WIDTH = 0.4
PROBE_LENGTH = 10
CRITICAL_ANGLE = np.arcsin(LASER_WIDTH / PROBE_LENGTH)

def angle_to_scattering_vector(angle):
    """

    Args:

    Returns:
    """
    return 4 * np.pi / LASER_WAVELENGTH * np.sin(np.radians(angle))

def footprint_correction(angles, intensity):
    """

    Args:

    Returns:
    """
    angle_cutoff_index = np.argmin(np.radians(angles) < CRITICAL_ANGLE)
    correction = np.linspace(0, 1, num=angle_cutoff_index)
    corrected_intensity = np.divide(intensity[:angle_cutoff_index], correction[:angle_cutoff_index])
    return np.concatenate((corrected_intensity, intensity[angle_cutoff_index:]))

def simulated_surface(scattering_vectors, sigma, offset, norm):
    """

    Args:

    Returns:
    """
    return np.log(norm * np.multiply(np.power(scattering_vectors, -4),
                                     np.exp(- np.power(scattering_vectors, 2) * sigma**2)) + offset)

def main():
    """

    Args:
        None
    Returns:
        None
    """
    data = np.genfromtxt('Messdaten/S22.txt').T
    angles = data[0]
    intensity = data[9]

    scattering_vectors = angle_to_scattering_vector(angles)
    intensity_footprint_corrected = footprint_correction(angles, intensity)

    # Fit
    cutoff = np.argmax(data[9] < 0.01 * np.max(data[9]))
    popt, pcov = curve_fit(simulated_surface,
                           scattering_vectors[cutoff:],
                           np.log(intensity_footprint_corrected[cutoff:]),
                           bounds=(0., [20,400,1000]))

    intensity_fit = np.exp(simulated_surface(scattering_vectors, *popt))[cutoff:]

    # Plot data and fit 
    #fig1 = plt.figure(figsize=(4, 3))
    #fig1.add_axes((.1, .3, .8, .6))
    plt.plot(scattering_vectors, intensity_footprint_corrected, label="data")
    plt.plot(scattering_vectors[cutoff:], intensity_fit, label="fit")

    plt.ylabel('Intensität $[$cps$]$', fontsize=15)
    plt.xlabel('Streuvektor $q_z\\: [1/\\AA]$', fontsize=15)
    plt.yscale("log")
    plt.legend()
    plt.savefig("bilder/plot_xrr_fit.pdf", bbox_inches="tight", dpi=300)
    plt.close()

    # Plot data and fit 
    #fig1 = plt.figure(figsize=(4, 3))
    #fig1.add_axes((.1, .3, .8, .6))
    plt.plot(scattering_vectors[cutoff:], intensity_fit)

    plt.ylabel('Intensität $[$cps$]$', fontsize=15)
    plt.xlabel('Streuvektor $q_z\\: [1/\\AA]$', fontsize=15)
    plt.yscale("log")
    plt.legend()
    plt.savefig("bilder/plot_xrr_just_fit.pdf", bbox_inches="tight", dpi=300)
    plt.close()

    # Residual plot
    plt.figure(figsize=(6,2))
    residual = intensity_footprint_corrected[cutoff:] - intensity_fit
    plt.plot(scattering_vectors[cutoff:], residual, marker='o', linestyle='None', label="residuum")
    plt.ylabel('Intensity $[$cps$]$', fontsize=15)
    plt.xlabel('$q$', fontsize=15)
    #plt.ylim(1, 1e5)
    plt.yscale("log")
    plt.legend()
    plt.savefig("bilder/plot_xrr_fit_residual.pdf", bbox_inches="tight", dpi=300)
    plt.close()
    
    # Pull plot
    """
    pull = residual / cross_section_err[:,i]
    plt.plot(scattering_vectors, pull, marker='o', linestyle='None')
    plt.ylabel('Intensity $[$cps$]$', fontsize=15)
    plt.xlabel('$q$', fontsize=15)
    plt.savefig("bilder/plot_xrr_fit_pull.pdf", dpi=300)
    plt.close()
    """

    # Print data
    perr = np.sqrt(np.diag(pcov))
    np.set_printoptions(precision=4, suppress=True)
    print("Cutoff: {}".format(cutoff))
    print("Critical angle: {}".format(CRITICAL_ANGLE))
    print("Sigma: {} +- {}".format(popt[0], perr[0]))
    print("Offset C: {} +- {}".format(popt[1], perr[1]))
    print("Norm N: {} +- {}".format(popt[2], perr[2]))
    print("Covariance matrix: {}".format(pcov))


if __name__ == "__main__":
    main()
