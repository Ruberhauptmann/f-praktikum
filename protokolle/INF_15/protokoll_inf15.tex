\documentclass[12pt]{article}

\include{preamble}

\graphicspath{{bilder/}}

\title{INF15: Bestimmung der Oberflächenrauheit mittels AFM und Röntgenreflektometrie}
\author{Tjark Sievers, Balduin Letzer}
\date{\today}

\begin{document}

\maketitle
\thispagestyle{empty}
\clearpage

\begin{abstract}
    In diesem Versuch wird die Oberflächenrauheit einer Strontiumtitanatprobe untersucht. Dazu wird die Oberfläche der Probe mit einem Rasterkraftmikroskop abgebildet und 
    in einem Röntgenreflektometer untersucht.
\end{abstract}
\newpage

\tableofcontents
\newpage

\section{Einleitung}
Es wird die Oberfläche einer Strontiumtitanatprobe untersucht mit dem Ziel die Rauheit der Oberfläche zu quantifizieren. Dazu wird zunächst mit einem Rasterkraftmikroskop ein Bild von 
einem Teil der Oberfläche aufgenommen und mit den verschiedenen Tools, die das Analyseprogramm Gwyddion bietet, vermessen. 
Außerdem wird die Probe mit einem Röntgenreflektometer untersucht, um einen Eindruck von der Rauheit über der ganzen Probe zu bekommen.

\section{Theoretische Grundlagen}

\subsection{Charakterisierung der Oberflächenrauheit} \label{sub:oberflächenrauheit}
Für alle Punkte \(x\) einer Oberfläche kann die Höhe \(z\) gemessen werden. Wie stark sich diese Höhen unterscheiden, bestimmt die Rauheit der Oberfläche.
Um die Rauheit zu quantisieren gibt es verschiedene Methoden.

Im einfachsten Fall kann die durchschnittliche Abweichung der Höhen vom Mittelwert der Höhen betrachtet werden:
\begin{align}
    r_a = \frac{1}{n}\sum_{i=1}^n \vert z(x_i)-\overline{z}\vert
\end{align}
wobei die mittlere Höhe durch
\begin{align}
    \overline{z} = \frac{1}{n}\sum_{i=1}^n z(x_i)    
\end{align}
gegeben ist. \(n\) ist die Anzahl der Punkte, deren Höhenwerte zur Berechnung benutzt wurden. 
Desweiteren kann die Wurzel der mittleren Quadraten der Höhendifferenzen berechnet werden. Diese hat den Vorteil, dass gerade Ausreißer in den Messwerten stärker gewichtet werden. Dies 
ist sinnvoll, da auch wenige Ausreißer die Rauheit deutlich erhöhen. Es gilt:
\begin{align}
    r_{rms} = \sqrt{\frac{1}{n}\sum_{i=1}^n (z(x_i)-\overline{z})^2}
\end{align}
\begin{figure}[H]
    \centering
    \begin{subfigure}{.32\linewidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/atomarGlatteFläche.png}
      \caption{Atomar glatte Oberfläche}
      \label{sfig:schemaAtomarGlatt}
    \end{subfigure}
    \begin{subfigure}{.32\textwidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/atomarRauheOberfläche.png}
      \caption{Oberfläche mit einzelnen atomaren Defekten}
      \label{sfig:schmeaAtoamrRau}
    \end{subfigure}
    \begin{subfigure}{.32\textwidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/facettierteoberflächeGrafik.png}
      \caption{Facettierte Oberfläche}
      \label{sfig:schemaFacetten}
    \end{subfigure}
    \begin{subfigure}{.32\textwidth}
        \centering
        \includegraphics[width=\textwidth]{bilder/statistischverteilteOberfläche.png}
        \caption{Statistisch raue Oberfläche}
        \label{sfig:schemaStatistischRau}
      \end{subfigure}
    \caption{Verschieden raue Oberflächen \cite{lakner_2020}}
    \label{fig:verschidenRaueOberflächen}
\end{figure}
Für eine atomar glatte Oberfläche wie in Abbildung \ref{sfig:schemaAtomarGlatt} gilt \(r_a = r_{rms} = 0\). Dies ist in Realität kaum machbar, da einzelne Defekte schon in der 
Herstellung der Probe auftreten können und nach einiger Zeit von alleine entstehen können, wodurch die Rauheit natürlich erhöht wird. Es ist außerdem sehr schwer ein Probe so zu 
präparieren, dass die Oberfläche aus einer einzigen atomaren Schicht besteht. Es entstehen dann Facetten auf der Oberfläche, wie in Abbildung \ref{sfig:schemaFacetten} zu sehen. 
Wird die Höhenverteilung in einem Histogramm dargestellt, so kann eine statistisch rauen Oberfläche wie in Abbildung \ref{sfig:schemaStatistischRau} durch eine 
Gaußverteilung angenähert werden: 
\begin{align}
    g(z(x_i)) = \frac{C_{Norm}}{\sigma\sqrt{2\pi}}\exp\left(-\frac{(z(x_i)-\overline{z})^2}{2\sigma^2}\right)
\end{align}
Werden dann \(C_{Norm}\) und \(\sigma\) so gewählt, dass die gefittete Gaußkurve die gleiche Fläche und Varianz wie die Höhenverteilung hat, dann gilt für die Varianz:
\(\sigma = r_{rms}\). 

So kann beschrieben werden, was für Höhen auf der Oberfläche vorkommen. Es ist nun zu betrachten, wie diese Höhen auf der Oberfläche verteilt sind. Dazu wird die Höhen-Höhen 
Korrelationsfunktion (HHCF) benutzt. Die HHCF wird durch die Höhendifferenz zweier Punkte als Funktion ihres Abstandes gegeben. Die eindimensionale HHCF entlang der 
x-Richtung ist definiert als:
\begin{align}
    H_x(\tau_x) = \frac{1}{N(M-m)}\sum_{l=1}^N\sum_{n=1}^{M-m}\left(z(x_{n+m},y_l)-z(x_n,y_l)\right)^2
\end{align}
Dabei gilt \(m = \frac{\tau_x}{\Delta x}\), mit dem Abtastintervall \(\Delta x\). Das Abtastintervall ist durch das Verhältnis der Bildgröße und Anzahl der Messpunkte gegeben.
Die  Gesamtzahl der Messpunkte ist durch \(N\) und \(M\) gegeben. Mit genügend Messwerten kann die HHCF dann mit einem Gaußschen Profil angenähert werden:
\begin{align}
    H_x(\tau_x) = 2\sigma^2\left(1-\exp\left(-\frac{\tau_x^2}{\xi^2}\right)\right)
\end{align}
Dabei ist \(\xi\) die Korrelationslänge. Für zwei Punkte, deren Abstand größer ist als diese Länge, besteht dann keine Korrelation mehr zwischen den jeweiligen Höhenwerten. 
Es gilt außerdem wieder \(\sigma = r_{rms}\).

\subsection{Röntgenreflektometrie} \label{subs:xrr_theorie}
Die Röntgenreflektometrie basiert auf der Reflexion (d.h. Ausfallswinkel = Einfallswinkel) von Röntgenstrahlen an Oberflächen. Es gibt also einen einfallenden
Röntgenstrahl mit Wellenzahl \(\vec{k}\) und einen reflektierten Strahl mit \(\vec{k}^{\prime}\). Gemessen wird hierbei die Intensität \(I\)
des reflektierten Röntgenstrahls als Funktion des Streuwinkels \(\theta\). Zur Vereinheitlichung der Messungen wird statt des reinen Winkels der Betrag des Streuvektors 
\(\vec{q}\) genutzt (siehe Abbildung \ref{fig:skizze_xrr}).

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\textwidth}
    \input{bilder/skizze_xrr.pdf_tex}
    \caption{Reflexion an einer Oberfläche}
    \label{fig:skizze_xrr}
\end{figure}

Für die Reflexion steht \(\vec{q}\) senkrecht auf der Oberfläche, im gewählten Koordinatensystem also parallel zur \(z\)-Achse. Deshalb können wir
\begin{equation}
    \vert \vec{q} \vert = \vert q_z \vert = q_z
\end{equation}
schreiben und mit geometrischen Überlegungen einen Zusammenhang zwischen der Wellenlänge des einfallenden Strahles \(\lambda = \frac{2\pi}{k}\) (mit \(k = \vert \vec{k} \vert\)), 
dem Reflexionswinkel \(\theta\) und \(q_z\) finden:
\begin{align}
    \sin{\theta} &= \frac{k}{\frac{1}{2}q_z} \\
    \implies q_z &= \frac{4\pi}{\lambda} \sin{\theta}
\end{align}
Die Betrachtung des Streuvektors anstatt des Streuwinkels hat den Vorteil, dass Ergebnisse von Messungen mit verschiedenen Röntgenquellen besser vergleichbar sind, da die
aufgetragenen Graphen dann unabhängig von der Wellenlänge des genutzten Röntgenlichts ist.

Für kleine Reflektivitäten kann die Intensität mithilfe der Born-Näherung berechnet werden. Es gilt dann:
\begin{equation}
    I(q_z) \propto \frac{1}{q_z^4} \left\vert \int \dv{\rho(z)}{z} \exp(i q_z z) \right\vert^2
\end{equation}
\(\rho(z)\) ist hier die Elektronendichte in Abhängigkeit von der Höhe. Für eine ideal glatte Oberflächen ist dies eine Stufenfunktion, für raue Oberflächen 
wird diese zur Errorfunktion \(\erf(\frac{z}{\sqrt{2}\sigma})\). Im Integral steht die Ableitung der Elektronendichte:
\begin{align}
    \dv{z} \Theta(z) &= \delta(z) \\
    \dv{z} \erf(\frac{z}{\sqrt{2}\sigma}) &= \exp(-\frac{1}{2} \left( \frac{z}{\sigma} \right)^2)
\end{align}
Für die ideal glatte Oberfläche berechnet sich also die Intensität \(I(q_z)\) zu:
\begin{equation}
    I(q_z) \propto \frac{1}{q_z^4} \left\vert \int \delta(z) \exp(i q_z z) \dd{z} \right\vert^2 = \frac{1}{q_z^4} \exp(0) = \frac{1}{q_z^4}
\end{equation}
Für raue Oberflächen ist das Integral ein Listenintegral, die Proportionalität ergibt sich dann zu:
\begin{equation} \label{eq:intensity_real_surface}
    I(q_z) \propto \frac{1}{q_z^4} \exp(- q_z^2 \sigma^2)
\end{equation}
Trägt man \(I\) als Funktion von \(q_z\) auf, ist diese \(q^{-4}\)-Abhängigkeit die obere Grenze, für raue Oberflächen liegen alle Graphen darunter, wie in Abbildung \ref{fig:ideal_xrr_curves} zu sehen.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{bilder/ideal_xrr_curves.pdf}
    \caption{Idealisierte Kurven für \(I(q_z)\) für verschiedene Werte von \(\sigma\)}
    \label{fig:ideal_xrr_curves}
\end{figure}
Das \(\sigma\) ist wieder die Oberflächenrauheit, es lässt sich also mithilfe eines Fits der Funktion \ref{eq:intensity_real_surface} an die Messdaten
die Oberflächenrauheit bestimmen. Für reale Proben muss allerdings noch beachtet werden, dass bei kleinen Winkeln nicht der ganze Röntgenstrahl auf die Probe
fällt und damit auch die gemessene Intensität bei einem Winkel von \(\ang{0}\) auch \(0\) ist und in ein Maximum steigt.

\subsection{Probe: Strontiumtitanat (STO)}
Für die Probe wird Strontiumtitanat (\(SrTiO_3\)) benutzt (siehe Abbildung \ref{fig:sto_skizze}). Die Probe liegt als Einkristall vor und hat eine \((1 0 0)\)-Orientierung.
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{sto_skizze.png}
    \caption{STO Kristall mit Kristallsymmetrie und atomarer Besetzung der Gitterpositionen (links) und Oberfläche einer \(TiO_2\) - terminierten Oberfläche (rechts) \cite{lakner_2020}}
    \label{fig:sto_skizze}
\end{figure}

Die Gitterkonstante von \(SrTiO_3\) beträgt \(a = \SI{3.905}{\angstrom}\)

\subsection{Berechnung der Gitterkonstante} \label{sub:gitterkonstante}
Praktisch ist es kaum möglich, die Probe so zu präparieren, dass die Oberfläche perfekt parallel zu den Kristallebenen liegt. Es kommt also zu einer leichten Verkippung der Oberfläche. 
Der Winkel dieser Verkippung ist der Miscut \(\alpha\). Es kommt dann zu einer Facettierung der Oberfläche, wie in Abbildung \ref{fig:skizze_facettierte_oberfläche} zu sehen.
Aus dieser Facettierung kann dann die Gitterkonstante der Probe berechnet werden.

Die Gitterkonstante \(a\) ist hierbei konstant und unabhängig von der konkreten Beschaffenheit der Oberfläche, während die Größen \(c\) und \(\alpha\) von der Genauigkeit des Schleifprozesses abhängig sind. Bei einer ideal glatten Oberfläche
ist \(c \to \infty\) und \(\alpha = 0\).
\begin{figure}[H]
    \centering
    \def\svgwidth{0.7\textwidth}
    \input{bilder/Stufenhöhegrafik.pdf_tex}
    \caption{Facettierte Oberfläche}
    \label{fig:skizze_facettierte_oberfläche}
\end{figure}
Sowohl \(\alpha\) als auch \(c\) lassen sich aus den Messdaten eines Rasterkraftmikroskops bestimmen, so ergibt sich dann die Gitterkonstante zu:
\begin{equation}
    a = c \sin\alpha
    \label{eq:gitterkonstante}
\end{equation}

\newpage
\section{Experimenteller Aufbau und Durchführung}

\subsection{Rasterkraftmikroskopie}

\subsubsection{Aufbau}
Ein Rasterkraftmikroskop besteht aus einer feinen Spitze, die an einem einige Mikrometer breiten und einige zehn Mikrometer langen Cantilever befestigt ist. Dieser
Cantilever ist reflektierend und wird mit einem Laserstrahl angestrahlt, sodass je nach Messmethode die mechanische Verbiegung oder die Schwingung des Cantilevers
gemessen werden kann.

\subsubsection{Funktionsweise}
Auf den Cantilever wird ein Laserstrahl fokussiert, der dann abhängig von der Verbiegung des Cantilevers in unterschiedlichen Winkeln reflektiert wird. Gemessen wird
dies an einer Photodiode, die aus 4 Einzeldioden besteht. Für eine einfaches Höhenprofil werden dann die beiden oberen und die beiden unteren Dioden jeweils 
zusammengeschaltet und detektieren damit die Bewegung des reflektierten Laserstrahls in einer Richtung. Es wird somit die Verbiegung des Cantilevers gemessen,
die u.a. vom Höhenprofil der Probe abhängt.

Die Informationen aus den Photodioden werden in einer Feedbackschleife dazu genutzt, die Bewegung des Cantilevers in z-Richtung zu steuern, sodass die Spitze nicht
in die Probe rammt. Diese Steuerung geschieht durch Piezokristalle, die sich beim Anlegen eines Spannungsgradienten ausdehnen oder zusammenziehen und so eine
sehr präzise Steuerung der Spitze in allen drei Raumrichtungen erlauben.

Die Rasterkraftmikroskop kann in verschiedenen Betriebsarten genutzt werden. Beim Kontaktmodus wird die Kraftwirkung zwischen AFM-Spitze und der Oberfläche konstant
gehalten, sodass die Spitze die Oberfläche dauerhaft berührt.

Häufiger genutzt wird der Tappingmodus, bei dem der Cantilever mit einer Frequenz zwischen \(50\) und \(\SI{500}{\kHz}\) (in Luft, in Flüssigkeiten etwa 
\(\SI{10}{ \kHz}\)) schwingt. Da die Probe bei jeder Oszillation nur kurz berührt wird, werden die Oberflächen weicher Proben anders als beim Kontaktmodus nicht
zerstört. Gemessen werden dann die Änderung der Schwingungsamplitude und der Phase zwischen Anregung und Schwingung beim Kontakt mit der Probe.


\subsection{Röntgenreflektometrie}
Da das Röntgenreflektometer zum Zeitpunkt des Praktikums umgebaut wurde, konnten wir keine Messung durchführen, stattdessen haben wir Messdaten einer früheren
Messung der STO-Probe zur Analyse erhalten.

\newpage
\section{Ergebnisse}

\subsection{Rasterkraftmikroskopie}

\subsubsection{Erste Darstellung und Korrekturen}
In diesem Versuch wurden zwei Datensätze E18 und E20 aufgenommen. Da sich aber Fremdpartikel auf der Oberfläche abgesetzt hatten, werden auch noch zwei weitere Datensätze P02 und P06 untersucht. Diese 
wurden an einem frisch präparierten Kristall aufgenommen. Für E20 und P02 wurde eine Fläche von \(\SI{500}{\nano \m} \times \SI{500}{\nano \m}\) vermessen und für E18 und 
P06 eine \(\SI{2}{\micro \m} \times \SI{2}{\micro \m}\) Fläche. Die Datensätze werden dann in das Analyseprogramm Gwyddion geladen (Abbildung \ref{fig:erste_Aufnahmen}).
\begin{figure}[H]
    \centering
    \begin{subfigure}{.4\linewidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/E18_roh.pdf}
      \caption{E18}
      \label{sfig:E18_roh}
    \end{subfigure}
    \begin{subfigure}{.4\textwidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/E20_roh.pdf}
      \caption{E20}
      \label{sfig:E20_roh}
    \end{subfigure}
    \begin{subfigure}{.4\textwidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/P02_roh.pdf}
      \caption{P02}
      \label{sfig:P02_roh}
    \end{subfigure}
    \begin{subfigure}{.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{bilder/P06_roh.pdf}
        \caption{P06}
        \label{sfig:P06_roh}
      \end{subfigure}
    \caption{Erste Messungen}
    \label{fig:erste_Aufnahmen}
\end{figure}
Wie man besonders in Abbildung \ref{fig:erste_Aufnahmen}\ref{sub@sfig:P06_roh} sieht, ist ein Farbgradient in den Bildern sichtbar, die Probe ist also gegenüber der Sonde verkippt gewesen.
Um diesen Messfehler auszugleichen, bietet Gwyddion die Option "Level data by mean plane substraction". Dieses Tool fittet eine Ebene an die Messwerte und zieht diese 
dann von den Messwerten ab und ebnet das Bild. Würde diese Korrektur nicht vorgenommen werden, würde die Verkippung mit in die Rauheit eingerechnet werden, was natürlich 
nicht sinnvoll ist.
\begin{figure}[H]
    \centering
    \begin{subfigure}{.32\linewidth}
        \centering
        \includegraphics[width=\textwidth]{bilder/P06_roh.pdf}
        \caption{Rohdaten}
        \label{sfig:P06_roh2}
      \end{subfigure}
    \begin{subfigure}{.32\linewidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/P06_plane.pdf}
      \caption{Level Data}
      \label{sfig:P06_plane}
    \end{subfigure}
    \begin{subfigure}{.32\textwidth}
      \centering
      \includegraphics[width=\textwidth]{bilder/P06_planeflatten.pdf}
      \caption{Level Data und Align Rows}
      \label{sfig:P06_planeflatten}
    \end{subfigure}
    \caption{Anwendung von Level Data und Align Rows auf die Rohdaten von P06}
    \label{fig:P06_korrektur}
\end{figure}
In Abbildung \ref{fig:P06_korrektur}\ref{sub@sfig:P06_plane} sieht man nun noch Streifen unterschiedlicher Helligkeiten. Diese entstehen durch kleine Veränderungen wie z.B. thermischen 
Drift, wodurch sich Sonde und Probe relativ zueinander bewegen. Die Streifen sind also keine Eigenschaft der Probe und müssen auch rausgerechnet werden. 
Dazu bietet Gwyddion das Tool ''Align rows using various Methods''. Mit diesem Tool werden die Mittelwerte der einzelnen Zeilen angeglichen, wodurch sich
Abbildung \ref{fig:P06_korrektur}\ref{sub@sfig:P06_planeflatten} ergibt. Es werden im folgenden alle Datensätze zuerst mit der Level Data- und dann mit der 
Align Rows-Funktion bearbeitet.

\subsubsection{Bestimmung der Oberflächenrauheit}
Es wird zunächst der Datensatz P06 betrachtet. Gwyddion bietet die Möglichkeit, direkt eine Sammlung von statistischen Größen des Datensatzes auszugeben. 
Es können so erste Messwerte für die Rauheit aufgenommen werden:
\begin{align}
    r_{rms} = \SI{138.5}{\pico m} , \hspace{5mm} r_a = \SI{112.3}{\pico m}
\end{align}
Bei diesen Werten muss nur beachtet werden, dass keine Fehler mitausgegeben werden.
Gwyddion kann außerdem verschiedene statistische Funktionen ausgeben. Davon werden die Höhenverteilung und HHCF betrachtet. 
Die Höhenverteilung hat die Form einer Gaußglocke, ein Fit ergibt dann die Rauheit \(\sigma = r_{rms}\). Es ist hierbei zu beachten, dass Gwyddion als Fitparameter erst \(\sqrt{2}\sigma\) ausgibt.
\begin{figure}[H]
    \centering
    \begin{subfigure}{.49\linewidth}
        \centering
        \includegraphics[width=\textwidth]{P06_höhenverteilung.pdf}
        \caption{Höhenverteilung}
        \label{sfig:P06_höhenverteilung}
      \end{subfigure}
    \begin{subfigure}{.49\linewidth}
      \centering
      \includegraphics[width=\textwidth]{P06_Gaußfit.pdf}
      \caption{Höhenverteilung mit Fit}
      \label{sfig:P06_höhenverteilung_fit}
    \end{subfigure}
    \caption{Höhenverteilung von P06 ohne und mit Fit}
    \label{fig:P06_höhenverteilung}
\end{figure}
Der Fit in Abbildung \ref{sfig:P06_höhenverteilung_fit} ergibt dann:
\begin{equation}
    \sigma_{HV} = \SI{146.7}{\pico m}\pm\SI{0.7}{\pico m}
\end{equation}

Es lässt sich außerdem die in Abschnitt \ref{sub:oberflächenrauheit} angesprochene Höhen-Höhen Korrelationsfunktion ausgeben.
Man sieht in Abbildung \ref{fig:P06_HHCF} eine Oszillation anstatt des Plateaus. Das liegt daran, dass es
durch die Periodizität der Facetten in regelmäßigen Abständen Punkte gleicher Höhe gibt. 
\begin{figure}[H]
    \centering
    \begin{subfigure}{.49\linewidth}
        \centering
        \includegraphics[width=\textwidth]{P06_HHCF_ganzes_bild_no_log.pdf}
        \caption{HHCF linear ohne Fit}
        \label{sfig:P06_HHCF_noLog}
      \end{subfigure}
    \begin{subfigure}{.49\linewidth}
      \centering
      \includegraphics[width=\textwidth]{P06_HHCF_ganzes_bild.pdf}
      \caption{HHCF mit Fit}
      \label{sfig:P06_HHCF_Fit}
    \end{subfigure}
    \caption{HHCF von P06 linear und logarithmisch dargestellt und mit Fit}
    \label{fig:P06_HHCF}
\end{figure}
Der Fit eines Gaußprofils in \ref{sfig:P06_HHCF_Fit} liefert \(\sigma_{HHCF}=\SI{139.9}{\pico m}\pm\SI{0.5}{\pico m}\).
Die HHCF liefert dazu noch die Korrelationslänge \(\xi = \SI{28}{\nano m}\pm \SI{2}{\nano m}\). 

In allen diesen Werten gehen aber auch die Facetten der Probenoberfläche ein. Es wird nun noch die Rauheit einer einzelnen Facette betrachtet. 
Dazu wird per Hand eine einzelne Facette markiert und dann durch Abzug eines Polynomfits 1. Grades geebnet. Es ergibt sich dann für eine einzelne Facette mit der 
gleichen Analyse wie für das Gesamtbild:
\begin{align}
    r_{rms} &= \SI{95.37}{\pico m}, \hspace{5mm} r_a = \SI{73.97}{\pico m} \\
    \sigma_{HV} &= \SI{89.0}{\pico m}\pm\SI{0.4}{\pico m} \\
    \sigma_{HHCF} &= \SI{95.8}{\pico m}\pm\SI{0.8}{pm}, \hspace{5mm} \xi = \SI{7.5}{\nano m}\pm \SI{7}{\nano m}
\end{align}
Damit ist auch die Oberflächenrauheit einer einzelnen Facette bestimmt.

Zuletzt kann durch die Stufenhöhe der Facetten die Gitterkonstante der Probe ermittelt werden (siehe Abschnitt \ref{sub:gitterkonstante}). 
Dazu wird zunächst das Tool ''Measure Facets'' benutzt um den 
Miscut zu messen. Es ergibt sich ein Winkel von \(\alpha = \SI{0.086}{\degree}\). Die Facettenbreite kann sowohl im Ortsraum als auch im reziproken 
Raum bestimmt werden. Im Ortsraum wird mit Gwyddion das Höhenprofil entlang einer Linie über die Stufen entnommen (Abbildung \ref{fig:P06_profil}).
\begin{figure}[H]
    \centering
    \begin{subfigure}{.49\linewidth}
        \centering
        \includegraphics[width=\textwidth]{P06_profillinie.pdf}
        \caption{Profillinie}
        \label{sfig:P06_profillinie}
      \end{subfigure}
    \begin{subfigure}{.49\linewidth}
      \centering
      \includegraphics[width=\textwidth]{P06_faccetten_profil.pdf}
      \caption{Höhenprofil über P06}
      \label{sfig:P06_facettenprofil}
    \end{subfigure}
    \caption{Profillinie und entsprechendes Höhenprofil über P06}
    \label{fig:P06_profil}
\end{figure}
 Die Breiten der Stufen werden dann gemittelt. Es werden über eine Strecke von \(\SI{1.81}{\micro\m}\) acht Stufen gezählt (es wird ein Ablesefehler von 
 \(\SI{0.01}{\micro\m}\) angenommen). Dadurch folgt für die Stufenhöhe:
 \begin{align}
     a = \frac{\SI{1.81}{\micro\m}}{8}\sin(\SI{0.086}{\degree}) = \SI{340}{\pico\m} \pm \SI{2}{\pico\m}
 \end{align} 
 In Gwyddion kann das Bild fouriertransformiert werden. Es ergibt sich Abbildung \ref{fig:P06_fourier}.
\begin{figure}[H]
    \centering
    \begin{subfigure}{.49\linewidth}
        \centering
        \includegraphics[width=\textwidth]{P06_fourier.pdf}
        \caption{Fouriertransformation von P06}
        \label{fig:P06_fourier}
      \end{subfigure}
    \begin{subfigure}{.49\linewidth}
      \centering
      \includegraphics[width=\textwidth]{P06_Fourier_abstandmessung.pdf}
      \caption{Höhenprofil über die Fouriertransformation}
      \label{sfig:P06_fourierProfil}
    \end{subfigure}
    \caption{Fouriertransformation und Höhenprofil}
    \label{fig:P06_Fourierprofil}
\end{figure}
Über den weißen Strich in der Mitte des Bildes kann wieder ein Profil entnommen werden. In diesem Profil sind zwei Peaks zu erkennen. 
Aus dem Abstand \(d = \SI{8.8}{\micro m}^{-1}\) dieser Peaks lässt sich dann die Breite der Facetten berechnen:  
\begin{align}
    c = \frac{1}{\frac{1}{2}d} 
\end{align} 
Damit ergibt sich für die Stufenhöhe:
\begin{align}
    a = \frac{2}{\SI{8.8}{\micro m}^{-1}}\sin(\SI{0.086}{\degree}) = \SI{341.1}{\pico m} \pm \SI{0.4}{\pico\m}
\end{align}
Zuletzt kann Gwyddion selbst die Stufenhöhen berechnen. Dazu wird da Tool ''Fit Facet'' benutzt. Dieses fittet Terrassen an die Facetten der Oberfläche. 
Dazu müssen die Parameter des Fits per Hand so angepasst werden, dass die gefitteten Terrassen mit den Tatsächlichen übereinstimmen. Dann ergeben sich Terrassen 
wie in Abbildung \ref{fig:P06Terrassenfit}.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.32\textwidth]{P06_terasen_fit.png}
    \caption{Terrassenfit an P06}
    \label{fig:P06Terrassenfit}
\end{figure}
Gwyddion kann dann die Stufenhöhe zwischen den Fitwerten ausgeben, es ergibt sich:
\begin{align}
    a = \SI{374.8}{\pico m}\pm\SI{0.7}{\pico m}
\end{align}
Die gleiche Analyse kann an P02 durchgeführt werden, allerdings muss beachtet werden, das die Aufnahme augrund des kleineren Ausschnitts für die Analyse 
einzelner Facetten besser geeignet ist als für die Analyse der gesamten Rauheit. Es ergibt sich dann:
\begin{table}[H]
    \centering
    \caption{Rauheitswerte für P06 und P02}
    \label{tab:werte_p}
    \begin{tabular}{L L L L}
     \hline 
     \hspace{5mm}                        &\hspace{5mm}             &\textnormal{P06}                       &\textnormal{P02}                        \\ \hline
     \multirow{9}{20mm}{Gesamtbild}      &r_{rms}                  &\SI{138.5}{\pico m}                    &\SI{152.8}{\pico m}                     \\ 
                                         &r_a                      &\SI{112.3}{\pico m}                    &\SI{119.6}{\pico m}                     \\ 
                                         &\sigma_{HV}              &\SI{146.7}{\pico m}\pm\SI{0.7}{\pico m}&\SI{148}{\pico m}\pm\SI{3}{\pico m}     \\ 
                                         &\sigma_{HHCF}            &\SI{139.9}{\pico m}\pm\SI{0.5}{\pico m}&\SI{164.6}{\pico m}\pm\SI{0.7}{\pico m} \\ 
                                         &\xi                      &\SI{28}{\nano m}\pm \SI{2}{\nano m}    &\SI{52}{\nano m}\pm \SI{1}{\nano m}     \\
                                         &\alpha                   &\SI{0.086}{\degree}                    &\SI{0.064}{\degree}                     \\ 
                                         &a_{\textnormal{Ortsraum}}&\SI{340}{\pico m}\pm\SI{2}{\pico\m}    &\SI{290}{\pico m}\pm\SI{10}{\pico\m}                    \\ 
                                         &a_{\textnormal{reziprok}}&\SI{341.1}{\pico m}\pm\SI{0.4}{\pico\m}&\SI{294}{\pico m}\pm\SI{4}{\pico\m}                  \\ 
                                         &a_{\textnormal{fit}}     &\SI{374.8}{\pico m}\pm\SI{0.7}{\pico m}&\SI{372.2}{\pico m}\pm\SI{0.8}{\pico m} \\ \hline
     \multirow{5}{20mm}{Einzelne Facette}&r_{rms}                  &\SI{95.37}{\pico m}                    &\SI{103.8}{\pico m}                     \\ 
                                         &r_a                      &\SI{73.97}{\pico m}                    &\SI{81.3}{\pico m}                      \\ 
                                         &\sigma_{HV}              &\SI{89.0}{\pico m}\pm\SI{0.4}{\pico m} &\SI{98.7}{\pico m}\pm\SI{0.4}{\pico m} \\ 
                                         &\sigma_{HHCF}            &\SI{95.8}{\pico m}\pm\SI{0.8}{\pico m} &\SI{98}{\pico m}\pm\SI{1}{\pico m}      \\ 
                                         &\xi                      &\SI{7.5}{\nano m}\pm \SI{0.7}{\nano m} &\SI{10}{\nano m}\pm \SI{2}{\nano m}     \\ \hline
    \end{tabular}  
\end{table}


Durch die Partikel, die sich auf der Probe vor der Aufnahme von E18 und E20 abgesetzt hatten, können nicht alle Teile der Analyse durchgeführt werden. 
Die Ermittlung der Stufenhöhe im Ortsraum und im reziproken Raum fällt bei beiden Datensätzen weg. Bei E20 kann aber direkt im Höhenprofil über 
eine Stufe die Stufenhöhe direkt gemessen werden. Dazu wird an jede einzelne Stufe eine lineare Funktion angefittet und der Abstand zwischen den Fits 
gemessen (Abbildung \ref{fig:E20Stufenfit}). 
\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{E20_HöhenprofilMitStufenFits.pdf}
    \caption{Messung der Stufenhöhe in E20}
    \label{fig:E20Stufenfit}
\end{figure}
Dabei ergibt sich dann für die Stufenhöhe:
\begin{align}
    a = \SI{398}{\pico m} \pm \SI{10}{\pico m}
\end{align} 
Es muss außerdem angemerkt werden, dass die Facet Fits nicht so genau sind wie für P02 und P06. Wie in Abbildung \ref{fig:gefittete_terrassen} zu sehen ist, sind die gefitteten Terrassen 
wesentlich ungleichmäßiger.
\begin{figure}[H]
    \centering
    \begin{subfigure}{.24\linewidth}
      \centering
      \includegraphics[width=\textwidth]{E18_terassenFit.png}
      \caption{E18}
      \label{sfig:E18_terrasse}
    \end{subfigure}
    \begin{subfigure}{.24\textwidth}
      \centering
      \includegraphics[width=\textwidth]{E20_terassenFit.png}
      \caption{E20}
      \label{sfig:E20_terrasse}
    \end{subfigure}
    \begin{subfigure}{.24\textwidth}
        \centering
        \includegraphics[width=\textwidth]{P06_terasen_fit.png}
        \caption{P06}
        \label{sfig:P06_terrasse}
      \end{subfigure}
    \begin{subfigure}{.24\textwidth}
      \centering
      \includegraphics[width=\textwidth]{P02_terasen_fit.png}
      \caption{P02}
      \label{sfig:P02_terrasse}
    \end{subfigure}
    \caption{Gefittete Terrassen der Datensätze}
    \label{fig:gefittete_terrassen}
\end{figure}

\newpage
Dadurch kommt es zu größeren Fehlern. Ansonsten kann wie für P06 und P02 analysiert werden: 
\begin{table}[H]
    \caption{Rauheitswerte aller Datensätze}
    \label{tab:alle_werte}
\begin{adjustwidth}{-35pt}{}
    \begin{tabular}{L L L L L L}
    \hline
                                        &                         &\textnormal{P06}                         &\textnormal{P02}                       &\textnormal{E18}                       &\textnormal{E20}                       \\ \hline
    \multirow{9}{10mm}{Gesamt-bild}     &r_{rms}                  &\SI{138.5}{\pico m}                      &\SI{152.8}{\pico m}                    &\SI{341.5}{\pico m}                    &\SI{266.3}{\pico m}                    \\ 
                                        &r_a                      &\SI{112.3}{\pico m}                      &\SI{119.6}{\pico m}                    &\SI{227.7}{\pico m}                    &\SI{207.0}{\pico m}                    \\ 
                                        &\sigma_{HV}              &\SI{146.7}{\pico m}\pm\SI{0.7}{\pico m}  &\SI{148}{\pico m}\pm\SI{3}{\pico m}    &\SI{232}{\pico m}\pm\SI{3}{\pico m}    &\SI{221}{\pico m}\pm\SI{3}{\pico m}    \\ 
                                        &\sigma_{HHCF}            &\SI{139.9}{\pico m}\pm\SI{0.5}{\pico m}  &\SI{164.6}{\pico m}\pm\SI{0.7}{\pico m}&\SI{342.2}{\pico m}\pm\SI{0.3}{\pico m}&\SI{268.9}{\pico m}\pm\SI{0.3}{\pico m}\\ 
                                        &\xi                      &\SI{28}{\nano m}\pm \SI{2}{\nano m}      &\SI{52}{\nano m}\pm \SI{1}{\nano m}    &\SI{10.3}{\nano m}\pm \SI{0.3}{\nano m}&\SI{7.7}{\nano m}\pm \SI{0.1}{\nano m} \\
                                        &\alpha                  &\SI{0.086}{\degree}                      &\SI{0.064}{\degree}                    & -                                     & -                                     \\ 
                                        &a_{\textnormal{Ortsraum}}&\SI{340}{\pico m}\pm\SI{2}{\pico\m}      &\SI{290}{\pico m}\pm\SI{10}{\pico\m}   & -                                     &\SI{398}{\pico m}\pm \SI{10}{\pico\m} \\ 
                                        &a_{\textnormal{reziprok}}&\SI{341.1}{\pico m}\pm\SI{0.4}{\pico\m}  &\SI{294}{\pico m}\pm\SI{4}{\pico\m}    & -                                     & -                                        \\ 
                                        &a_{\textnormal{fit}}     &\SI{374.8}{\pico m}\pm\SI{0.7}{\pico m}  &\SI{372.2}{\pico m}\pm\SI{0.8}{\pico m}&\SI{347}{\pico m}\pm\SI{1}{\pico m}    &\SI{361}{\pico m}\pm\SI{1}{\pico m}    \\ \hline
    \multirow{5}{20mm}{Einzelne Facette}&r_{rms}                  &\SI{95.37}{\pico m}                      &\SI{103.8}{\pico m}                    &\SI{270.5}{\pico m}                    &\SI{240.6}{\pico m}                    \\ 
                                        &r_a                      &\SI{73.97}{\pico m}                      &\SI{81.3}{\pico m}                     &\SI{204.6}{\pico m}                    &\SI{185.2}{\pico m}                    \\ 
                                        &\sigma_{HV}              &\SI{89.0}{\pico m}\pm\SI{0.4}{\pico m}   &\SI{98.7}{\pico m}\pm\SI{0.4}{\pico m} &\SI{197}{\pico m}\pm\SI{4}{\pico m}    &\SI{194}{\pico m}\pm\SI{3}{\pico m}    \\ 
                                        &\sigma_{HHCF}            &\SI{95.8}{\pico m}\pm\SI{0.8}{\pico m}   &\SI{98}{\pico m}\pm\SI{1}{\pico m}     &\SI{267.1}{\pico m}\pm\SI{0.8}{\pico m}&\SI{238.0}{\pico m}\pm\SI{0.3}{\pico m}\\ 
                                        &\xi                      &\SI{7.5}{\nano m}\pm \SI{0.7}{\nano m}   &\SI{10}{\nano m}\pm \SI{2}{\nano m}    &\SI{6.1}{\nano m}\pm\SI{0.3}{\nano m}  &\SI{6.0}{\nano m}\pm\SI{0.1}{\nano m}  \\ \hline
    \end{tabular} 
\end{adjustwidth}
\end{table}


\subsection{Röntgenreflektometrie}
Das Röntgenreflektometer gibt den incident angle \(\theta\) aus, mit der Wellenlänge \(\lambda = \SI{0.71}{\angstrom}\) der genutzten Molybdän-Röntgenquelle ergibt
sich dann \(q_z\) zu:
\begin{equation}
    q_z = \frac{4\pi}{\SI{0.71}{\angstrom}} \sin\theta
\end{equation}
Aufgrund der endlichen Ausdehnung von Laser und Probe müssen die gemessenen Intensitäten bis zu einem gewissen Winkel korrigiert werden. Dieser Winkel \(\theta_b\), ab dem das gesamte
Laserlicht auf die Probe fällt ergibt sich aus geometrischen Überlegungen aus der Breite des Laserstrahls \(b = \SI{0.4}{\mm}\) und der Länge der Probe \(L = \SI{10}{\mm}\).
\begin{equation}
    \theta_b = \arcsin \frac{b}{L}
\end{equation}
Für die Korrektur werden die gemessenen Intensitäten (von 0 bis zum Winkel \(\theta_b\)) durch eine linear von 0 bis 1 ansteigende Korrekturfunktion geteilt.

In Abbildung \ref{fig:xrr_data} ist die gemessene Intensität sowie die Korrektur dargestellt.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{plot_xrr.pdf}
    \caption{Absorber-korrigierte XRR-Daten}
    \label{fig:xrr_data}
\end{figure}

Es wird Python benutzt um mit einem non-linear least square fit die Parameter \(N, C\) und vor allem die Rauheit \(\sigma\) der Funktion
\begin{equation} \label{eq:fitting_function}
    I(q_z) = N \frac{1}{q_z^4} \exp(- q_z^2 \sigma^2) + C
\end{equation}
zu finden und an die Messdaten zu fitten. Hierbei wird vor dem Fitten der Logarithmus der Funktion \ref{eq:fitting_function} und der Messdaten genommen. Außerdem werden
die ersten 48 Messwerte im Fit nicht berücksichtigt, da die Born-Näherung erst bei ca. \(1\%\) der maximalen Intensität gilt.

Damit ergibt sich dann der in Abbildung \ref{fig:xrr_data_fit} abgebildete Fit.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{plot_xrr_fit.pdf}
    \caption{XRR-Daten mit fit}
    \label{fig:xrr_data_fit}
\end{figure}
Aus dem Fit ergeben sich dann:
\begin{align}
    \sigma &= \SI{163}{\pico\metre} \pm \SI{6}{\pico\metre} \\
    C &= 11.6 \pm 0.6 \\ %Einheit?
    N &= 6.9 \pm 0.2
\end{align}

\newpage
\section{Diskussion}
\subsection{Interpretation der Werte}
Anhand von Tabelle \ref{tab:alle_werte} lassen sich die verschiedenen Messungen und Methoden der Auswertung im AFM-Versuch gut vergleichen. Ganz allgemein sind
die Rauheitswerte bei den Aufnahmen E18 und E20 um einen Faktor \(\geq 2\) größer als bei den Aufnahmen P06 und P02. Dies spricht für die bereits bei der Aufnahme
von E18 und E20 aufkommende Vermutung, dass die Probe durch Fremdatome verschmutzt war.

An den Messungen E18 und E20 lässt sich auch gut die unterschiedliche Bedeutung von \(r_{rms}\) und \(r_a\) erläutern: In Abbildung \ref{fig:erste_Aufnahmen}
ist zu erkennen, dass es einige besonders helle Punkte gibt, also einige im Vergleich hohe Erhebungen. Diese Ausreißer werden von \(r_{rms}\) mehr gewichtet,
sodass sich hier bei E18 und E20 ein deutlicher Unterschied von \(\sim \SI{100}{\pico\m}\) ergibt, während bei den P06 und P02 Aufnahmen kaum Ausreißer zu sehen
sind, weswegen sich die Rauheitswerte \(r_{rms}\) und \(r_a\) nur um \(\sim \SI{30}{\pico\m}\) unterscheiden.

Auch die Unterschiede zwischen der Messung des Gesamtbildes und einer einzelnen Facette lassen sich hier gut erläutern. Während bei der Messung von Rauheitswerten
über das gesamte Bild die Facetten Höhenunterschiede zwischen Punkten verursachen, werden diese bei der Messung einer einzelnen Facette ausgeblendet. Dadurch sind die
Rauheitswerte bei den Messungen einzelner Facetten in allen Messreihen geringer als bei der Messung des gesamten Bildes.

Die errechneten Gitterkonstanten liegen alle nah an der realen Gitterkonstante des STO-Kristalls. 

Die durch die XRR-Analyse errechnete Rauheit liegt nah an den Rauheitswerten der AFM-Analyse von P06. Dies zeigt zum einem, dass die Probe bei der XRR-Messung
und der AFM-Analyse ähnlich rein war und zum anderen, dass der Teil der Oberfläche, der in P06 vermessen wurde bereits einen guten Überblick über die gesamte 
Oberfläche gibt, die mit der XRR-Analyse ausgemessen wird.

\subsection{Fehlerdiskussion}
\subsubsection{Rauschen}
Die Nadel wird mit piezoelektrischen Bauteilen bewegt. Die Spannung, die diese Bauteile betreibt, hat ein geringes Rauschen.
Dies führt dennoch zu einem Wackeln der Spitze um wenige Picometer, was sich als Rauschen im Bild wiederspiegelt.

\subsubsection{Verkippung}
Aufgrund der Präparation von Spitze und Probe ist es nicht möglich, vollkommen senkrecht mit der Spitze auf der Probe zu messen. Dadurch muss die Spitze weiterausgefahren
werden um die von der Sonde weggekippten Seite zu vermessen, was zu einem Farbgradient im Bild führt. 

\subsubsection{Thermischer Drift}
Das AFM besteht aus vielen verschiedenen Materialien, welche alle verschiedene Wärme-ausdehnungskoeffizienten haben. 
Dadurch sind bei konstanter Steuerspannung, die Position der Probe und der Tunnelspitze relativ zueinander nie komplett fixiert. 
Durch kleine Temperaturschwankungen in der Umgebung des AFM, kann es im Laufe einer Messung passieren, dass sich 
die Probe unter der Spitze verschiebt. Dies führt zu Helligkeitsunterschieden entlang der y-Richtung. 

\subsubsection{Piezo-Creep}
Wird die Spannung an einem piezoelektrisches Element schlagartig umpolarisiert, kann das Element seine Auslenkung nicht sofort vollständig umkehren, es driftet nach 
der Änderung noch eine kleine Strecke weiter in die Richtung der vorigen Spannung.
Genau dies passiert, wenn nach einer Messung in einer Richtung das AFM beginnt, in die entgegengesetzte Richtung zu messen. Dies führt dazu, dass zu Beginn der nächsten Messung,
die Spitze zwar in die eine Richtung bewegt wird, die Piezoelemente aber noch in die entgegengesetzte Richtung driften und sich die beiden Bewegungen überlagern. 
Dadurch kommt es gerade am unteren Rand des Bildes zu Verzerrungen.

\subsubsection{Menschlicher Fehler}
Viele Messmethode sind stark abhängig vom Durchführenden der Messung. Dies beginnt mit der Auswahl des Bereichs auf der Probenoberfläche, der im AFM untersucht werden soll. 
Wird hier ein stark verschmutzter Bereich gewählt, kann keine Aussage mehr über die tatsächliche Oberfläche getroffen werden. In Gwyddion werden die Terrassenfits zur Vermessung von 
Stufenhöhe oder Facettenverkippung so gewählt, dass die Masken zur Markierung der Facetten möglichst genau die unterschiedlichen Facetten abgrenzen. Wann diese Masken allerdings am besten passen, 
ist sehr subjektiv und es kann so wieder die Messung verfälscht werden. 

\newpage
\section{Zusammenfassung}
In diesem Versuch wurde die Oberflächenrauheit einer Strontiumtitanatprobe gemessen. Dazu wurde die Oberfläche mit einem Rasterkraftmikroskop und einem Röntgenreflekto-meter untersucht.
Es wurden dabei mit dem AFM einmal eine frisch präparierte Probe und eine Probe, auf der sich Fremdpartikel abgesetzt hatten vermessen. Die damit aufgenommenen Bilder wurden dann in Gwyddion 
ausgewertet. Es wurden die verschiedenen Tools die Gwyddion bereitstellt benutzt um die bekannten Rauheitswerte mit unterschiedlichen Methoden zu ermitteln. Die durch die XRR gegebenen 
Messwerte wurden in Python ausgewertet und gefittet um einen Rauheitswert für die gesamte Oberfläche zu finden. 

Die AFM-Messung ergab dann Rauheitswerte im Bereich von \(100-\SI{350}{\pico m}\) und die XRR-Analyse eine Rauheit von \(\SI{163}{\pico m}\pm\SI{6}{\pico m}\).

\newpage
\thispagestyle{empty}
\bibliography{protokoll_inf15}
\bibliographystyle{abbrv}
\addcontentsline{toc}{section}{Literatur}

\end{document}
