import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import matplotlib
matplotlib.rcParams.update({'font.size': 12})

def ideal_surface(q):
    return np.power(q,-4)

def real_surface(q,sigma,offset,norm):
    return norm * np.multiply(np.power(q,-4),np.exp(- np.power(q,2) * sigma**2)) + offset

q_data = np.linspace(0,0.5,num=101)

offset = 5
norm = 1

#plt.figure(figsize=(4,3))
plt.plot(q_data, ideal_surface(q_data), label="Glatte Oberfläche")
#plt.plot(q_data,real_surface(q_data,1,offset,norm),label="1 Angström")
#plt.plot(q_data,real_surface(q_data,2,offset,norm),label="2 Angström")
plt.plot(q_data, real_surface(q_data,2,offset,norm), label="$\sigma = $2 Angström")
plt.plot(q_data, real_surface(q_data,5,offset,norm), label="$\sigma = $5 Angström")
plt.plot(q_data, real_surface(q_data,10,offset,norm), label="$\sigma = $10 Angström")

plt.yscale('log')

plt.xlabel("Streuvektor $q_z\\: [1/\\AA]$")
plt.ylabel("Intensität $I\\: [cps]$")

plt.legend()
#plt.show()
#plt.savefig('data.pdf',facecolor='white',dpi=900,bbox_inches='tight')
plt.savefig("bilder/ideal_xrr_curves.pdf", dpi=900, bbox_inches="tight")