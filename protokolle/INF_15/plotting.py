import matplotlib.pyplot as plt
import matplotlib
import numpy as np
matplotlib.rcParams.update({'font.size': 16})

hhcf_files = ["E18_HHCF_ganzesBild","E20_HHCF_einzelneFacette","E20_HHCF_ganzesBild","P02_HHCF_ganzesBild","P06_HHCF_Facette","P06_HHCF_ganzes_bild"]
#hhcf_files = ["E18_HHCF_ganzesBild"]
höhenverteilung_files = ["E18_Höhenverteilung_mitFit","E20_höhenverteilungMitFit","P02_höhenverteilung","P06_höhenverteilung","P02_gaußfit","P06_Gaußfit"]
residual_files = ["P02_gaußfit", "P06_Gaußfit", "E18_Höhenverteilung_mitFit", "E20_höhenverteilungMitFit"]
#höhenverteilung_files = ["E18_Höhenverteilung_mitFit"]
fouriermessungen_files = ["P02_fourier_abstandsmessung","P06_Fourier_abstandmessung"]
#fouriermessungen_files = ["P02_fourier_abstandsmessung"]

for files in residual_files:
    data = np.genfromtxt("bilder/Graphen/" + files + ".csv",delimiter=";").T

    plot_data = [[], [], []]
    conversion_factor = 9
    plot_data[0] = np.multiply(data[0],10**conversion_factor)
    plot_data[1] = np.multiply(data[1],10**(-conversion_factor))
    plot_data[2] = np.multiply(data[3],10**(-conversion_factor))

    residual = plot_data[1] - plot_data[2]

    plt.figure(figsize=(6,2))
    plt.plot(plot_data[0], residual, marker='o', linestyle='None', label="residuum")
    plt.xlabel("$z\\: [nm]$")
    plt.ylabel("$\\rho\\: [nm^{-1}]$")
    plt.legend()
    plt.savefig("bilder/" + files + "_residual.pdf", dpi=300, bbox_inches="tight")
    plt.close()


for files in hhcf_files:
    data = np.genfromtxt("bilder/Graphen/" + files + ".csv",delimiter=";").T
    # Convert data to right units
    plot_data = [[],[],[],[]]
    conversion_factor = 9
    plot_data[0] = np.multiply(data[0],10**conversion_factor)
    plot_data[1] = np.multiply(data[1],10**(2*conversion_factor))
    plot_data[2] = np.multiply(data[2],10**conversion_factor)
    plot_data[3] = np.multiply(data[3],10**(2*conversion_factor))

    # Plot mit m und m^2
    #plt.loglog(data[0][4:],data[1][4:],label="data")
    #plt.loglog(data[2][4:],data[3][4:],"r",label="fit")
    # Plot mit converted
    plt.loglog(plot_data[0][4:],plot_data[1][4:],label="data")
    plt.loglog(plot_data[2][4:],plot_data[3][4:],"r",label="fit")

    plt.xlabel("$\\tau\\:[nm]$")
    plt.ylabel("$H\\: [nm^2]$")
    plt.legend()

    #plt.show()
    plt.savefig("bilder/" + files + ".pdf",dpi=900,bbox_inches="tight")
    plt.close()

    residual = plot_data[1] - plot_data[3]
    plt.figure(figsize=(6,2))
    plt.plot(plot_data[0], residual, marker='o', linestyle="None", label="residuum")

    plt.xlabel("$\\tau\\:[nm]$")
    plt.ylabel("$H\\: [nm^2]$")
    plt.xscale("log")
    #plt.yscale("log")
    plt.legend()

    plt.savefig("bilder/" + files + "_residual.pdf", dpi=900, bbox_inches="tight")
    plt.close()


for files in höhenverteilung_files:
    data = np.genfromtxt("bilder/Graphen/" + files + ".csv",delimiter=";").T
    # Convert data to right units
    plot_data = [[],[],[],[]]
    conversion_factor = 9
    plot_data[0] = np.multiply(data[0],10**conversion_factor)
    plot_data[1] = np.multiply(data[1],10**(-conversion_factor))
    try:
        plot_data[2] = np.multiply(data[2],10**conversion_factor)
        plot_data[3] = np.multiply(data[3],10**(-conversion_factor))
    except IndexError:
        print("2 ist not defined")

    # Plot mit m und m^2
    """
    plt.plot(data[0][4:],data[1][4:],label="data")
    try:
        plt.plot(data[2][4:],data[3][4:],"r",label="fit")
    except IndexError:
        print("2 ist not defined")
    """
    # Plot mit converted
    plt.plot(plot_data[0][4:],plot_data[1][4:],label="data")
    try:
        plt.plot(plot_data[2][4:],plot_data[3][4:],"r",label="fit")
    except:
        print("2 ist not defined")

    #plt.ylim((10**-21,10**-19))
    #plt.xlim((10**-9,10**-5))
    plt.xlabel("$z\\: [nm]$")
    plt.ylabel("$\\rho\\: [nm^{-1}]$")
    plt.legend()

    #plt.show()
    plt.savefig("bilder/" + files + ".pdf",dpi=900,bbox_inches="tight")
    plt.close()


for files in fouriermessungen_files:
    data = np.genfromtxt("bilder/Graphen/" + files + ".csv",delimiter=";").T
    # Convert data to right units
    plot_data = [[],[],[],[]]
    conversion_factor = 9
    plot_data[0] = np.multiply(data[0],10**(-conversion_factor))
    plot_data[1] = np.multiply(data[1],10**conversion_factor)

    # Plot mit 
    plt.plot(plot_data[0][4:],plot_data[1][4:],label="data")
    # Plot mit converted
    #plt.loglog(plot_data[0][4:],plot_data[1][4:],label="data")
    #plt.loglog(plot_data[2][4:],plot_data[3][4:],"r",label="fit")

    #plt.ylim((10**-21,10**-19))
    #plt.xlim((10**-9,10**-5))
    plt.xlabel("$k\\: [nm^{-1}]$")
    plt.ylabel("$y\\: [nm]$")
    plt.legend()

    #plt.show()
    plt.savefig("bilder/" + files + ".pdf",dpi=900,bbox_inches="tight")
    plt.close()

## E20 Stufenfit
files = "E20_HöhenprofilMitStufenFits"
data = np.genfromtxt("bilder/Graphen/" + files + ".csv",delimiter=";").T
# Convert data to right units
plot_data = []
conversion_factor = 9
for i in range(0,6):
    plot_data.append(np.multiply(data[i],10**conversion_factor))

# Plot mit 
#plt.plot(data[0][4:],data[1][4:],label="data")
#plt.plot(data[2][4:],data[3][4:],"r",label="fit Stufe 1")
#plt.plot(data[4][4:],data[5][4:],label="fit Stufe 2")
# Plot mit converted
plt.plot(plot_data[0][4:],plot_data[1][4:],label="data")
plt.plot(plot_data[2][4:],plot_data[3][4:],"r",label="fit Stufe 1")
plt.plot(plot_data[4][4:],plot_data[5][4:],label="fit Stufe 2")
#plt.loglog(plot_data[0][4:],plot_data[1][4:],label="data")
#plt.loglog(plot_data[2][4:],plot_data[3][4:],"r",label="fit")

#plt.ylim((10**-21,10**-19))
#plt.xlim((10**-9,10**-5))
plt.xlabel("$s\\: [nm]$")
plt.ylabel("$z\\: [nm]$")
plt.legend()

#plt.show()
plt.savefig("bilder/" + files + ".pdf",dpi=900,bbox_inches="tight")
plt.close()

## P06 Facettenprofil
files = "P06_faccetten_profil"
data = np.genfromtxt("bilder/Graphen/" + files + ".csv",delimiter=";").T
# Convert data to right units
plot_data = []
conversion_factor = 9
for i in range(0,2):
    plot_data.append(np.multiply(data[i],10**conversion_factor))

# Plot mit 
plt.plot(plot_data[0][4:],plot_data[1][4:],label="data")
# Plot mit converted
#plt.loglog(plot_data[0][4:],plot_data[1][4:],label="data")
#plt.loglog(plot_data[2][4:],plot_data[3][4:],"r",label="fit")

#plt.ylim((10**-21,10**-19))
#plt.xlim((10**-9,10**-5))
plt.xlabel("$s\\: [nm]$")
plt.ylabel("$z\\: [nm]$")
plt.legend()

#plt.show()
plt.savefig("bilder/" + files + ".pdf",dpi=900,bbox_inches="tight")
plt.close()


## P06 ganzes Bild no log
files = "P06_HHCF_ganzes_bild"
data = np.genfromtxt("bilder/Graphen/" + files + ".csv",delimiter=";").T
# Convert data to right units
plot_data = [[],[]]
conversion_factor = 9
plot_data[0] = np.multiply(data[0],10**conversion_factor)
plot_data[1] = np.multiply(data[1],10**(2*conversion_factor))

# Plot mit 
#plt.plot(data[0][4:],data[1][4:],label="data")
# Plot mit converted
plt.plot(plot_data[0][4:],plot_data[1][4:],label="data")
#plt.loglog(plot_data[2][4:],plot_data[3][4:],"r",label="fit")

#plt.ylim((10**-21,10**-19))
#plt.xlim((10**-9,10**-5))
plt.xlabel("$\\tau\\: [nm]$")
plt.ylabel("$H\\: [nm^2]$")
plt.legend()

#plt.show()
plt.savefig("bilder/" + files + "_no_log.pdf",dpi=900,bbox_inches="tight")
plt.close()